//
//  ViewController.swift
//  Assignment3
//
//  Created by Temp on 01/02/22.
//

import UIKit

class OneController: UIViewController {
    
    let fifthVC = FourController()
    let studentListTableView = UITableView()
    var contacts =  ["Arjun","Agilaan","Navanee","Jayasuriya","Mani","Karthi","Vijay","Vishnu","Ragu","Vikram","Vignesh","Ajith","Prem"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "ONE"
        view.backgroundColor = .green
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(presentFour))
        studentListTableView.delegate = self
        studentListTableView.dataSource = self
        contacts.sort()
        studentListTableView.register(UITableViewCell.self, forCellReuseIdentifier: "ContactCell")
        view.addSubview(studentListTableView)
        
    }
    
    override func viewDidLayoutSubviews() {
        studentListTableView.frame = view.bounds
    }
    
    @objc func presentFour(){
        
        let fourNavVc = UINavigationController(rootViewController: fifthVC)
        
        fifthVC.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .close, target: self, action: #selector(dissmissFourVc))
        fifthVC.view.backgroundColor = .blue
        
        fourNavVc.modalPresentationStyle = .fullScreen
        present(fourNavVc, animated: true, completion: nil)
    }
    
    @objc func dissmissFourVc(){
        fifthVC.dismiss(animated: true, completion: nil)
    }
    
}

//MARK: - OneController Delegates
extension OneController: UITableViewDataSource,UITableViewDelegate{
    
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 2
//    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Section: \(section+1)"
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactCell", for: indexPath)
        cell.textLabel?.text = contacts[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = FourController()
        vc.myTitle = contacts[indexPath.row]
        vc.view.backgroundColor = .white
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}



class TwoController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "TWO"
        view.backgroundColor = .red
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .play, target: self, action: #selector(pushView))
        
    }
    
    @objc func pushView(){
        let vc = FourController()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
}

class ThreeController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "THREE"
        view.backgroundColor = .yellow
        
    }
    
    
}

class FourController: UIViewController {
    
    var myTitle = "FOUR"

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = myTitle
        view.backgroundColor = .orange
    }
    
}


