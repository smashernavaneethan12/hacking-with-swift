//
//  ViewController.swift
//  ToDoList
//
//  Created by Temp on 18/02/22.
//

import UIKit
import FirebaseDatabase

protocol AddTaskDelegate: AnyObject {
    func addTaskDelegateReturn(task: String)
}

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource, AddTaskDelegate {
    
    
    var tasks = [String]()
    var userDefaultTasks = [String]()
    var firebaseData: [String:Any] = [:]
    
    
    let taskTable = UITableView()
    let addTaskVc = AddTaskViewController()
    
    let deleteAllTasks: UIButton = {
        let button = UIButton()
        button.isHidden = false
        button.setTitle("Delete All Tasks", for: .normal)
        button.setTitleColor(.systemRed, for: .normal)
        button.backgroundColor = .clear
        button.layer.cornerRadius = 10
        return button
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        tasks = UserDefaults.standard.stringArray(forKey: "myTasks") ?? []
        //tasks.append("Hello")
        self.navigationController?.navigationBar.prefersLargeTitles = true
        taskTable.delegate = self
        taskTable.dataSource = self
        view.addSubview(taskTable)
        view.addSubview(deleteAllTasks)
        if tasks.count == 0 {
            deleteAllTasks.isHidden = true
        }
        
        view.backgroundColor = .systemBackground
        taskTable.register(UITableViewCell.self, forCellReuseIdentifier: "taskCells")
        
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addTaskPressed))
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .camera, target: self, action: nil)
        deleteAllTasks.addTarget(self, action: #selector(deleteAllPressed), for: .touchUpInside)
        
        addConstraint()
    }
    
    func fetchFireBaseData() async -> [String:Any]?{
        let ref = Database.database().reference()
        var safeData:[String:Any]?
        ref.child("employee").observeSingleEvent(of: .value) { snapShot in
            if let data = snapShot.value as? [String:Any]{
                safeData = data
                print(data)
            }
        }
        return safeData
    }
    
    
    //adding task func
    @objc func addTaskPressed(){
        addTaskVc.delegate = self
        addTaskVc.enterTextField.becomeFirstResponder()
        addTaskVc.view.isUserInteractionEnabled = true
        
        let singleTapGesture = UITapGestureRecognizer(target: self, action: #selector(singleTapGestureTapped))
        singleTapGesture.numberOfTapsRequired = 1
        addTaskVc.view.addGestureRecognizer(singleTapGesture)
        let nav = UINavigationController(rootViewController: addTaskVc)
        nav.modalPresentationStyle = .fullScreen
        self.present(nav, animated: true, completion: nil)
    }
    
    @objc func singleTapGestureTapped(){
        print("Tapped")
        DispatchQueue.main.async {
            self.addTaskVc.enterTextField.resignFirstResponder()
        }
    }
    
    @objc func deleteAllPressed(){
        
        let deleteAllAlert = UIAlertController(title: "Are you sure to delete all tasks?", message: "", preferredStyle: .alert)
        deleteAllAlert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { action in
            self.tasks.removeAll()
            self.userDefaultTasks = self.tasks
            UserDefaults.standard.setValue(self.userDefaultTasks, forKey: "myTasks")
            self.taskTable.reloadData()
            self.deleteAllTasks.isHidden = true
        }))
        deleteAllAlert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        self.present(deleteAllAlert, animated: true)
    }
    
    func addTaskDelegateReturn(task: String) {
        if task == "" || tasks.contains(task){
            return
        }else{
            userDefaultTasks = UserDefaults.standard.stringArray(forKey: "myTasks") ?? []
            userDefaultTasks.append(task)
            UserDefaults.standard.setValue(userDefaultTasks, forKey: "myTasks")
            tasks.append(task)
            taskTable.reloadData()
            if tasks.count != 0 {
                deleteAllTasks.isHidden = false
            }
            print(tasks)
        }
    }
    
    func addConstraint(){
        taskTable.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            taskTable.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            taskTable.bottomAnchor.constraint(equalTo: deleteAllTasks.topAnchor),
            taskTable.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            taskTable.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor)
            
        ])
        
        
        deleteAllTasks.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            deleteAllTasks.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0),
            deleteAllTasks.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -50),
            //                    cancelButton.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.5),
            //                    cancelButton.heightAnchor.constraint(equalToConstant: 40)
        ])
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasks.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = taskTable.dequeueReusableCell(withIdentifier: "taskCells", for: indexPath)
        cell.textLabel?.text = tasks[indexPath.row]
        cell.textLabel?.textColor = .systemBlue
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        taskTable.deselectRow(at: indexPath, animated: true)
        let actionSheet = UIAlertController(title: tasks[indexPath.row], message: "Edit or Delete", preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Edit", style: .default, handler: { action in
            print("Editing...")
            let editAlert = UIAlertController(title: "EDIT",
                                              message: "",
                                              preferredStyle: .alert)
            
            editAlert.addTextField { textField in
                textField.text = self.tasks[indexPath.row]
            }
            editAlert.addAction(UIAlertAction(title: "Done", style: .default, handler: { action in
                //                guard let self = self else{
                //                    return
                //                }
                self.tasks[indexPath.row] = (editAlert.textFields?.first?.text)!
                self.taskTable.reloadData()
                self.userDefaultTasks = self.tasks
                UserDefaults.standard.setValue(self.userDefaultTasks, forKey: "myTasks")
                print(self.tasks)
                print("edit")}))
            
            editAlert.addAction(UIAlertAction(title: "Cancel",
                                              style: .cancel, handler: nil))
            self.present(editAlert, animated: true)
            
            
        }))
        actionSheet.addAction(UIAlertAction(title: "Delete",
                                            style: .destructive,
                                            handler: { [weak self] action in
            guard let self = self else{
                return
            }
            print("Deleting...")
            self.tasks.remove(at: indexPath.row)
            self.userDefaultTasks = self.tasks
            UserDefaults.standard.setValue(self.userDefaultTasks, forKey: "myTasks")
            self.taskTable.reloadData()
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            print("Canceled...")
        }))
        present(actionSheet, animated: true)
        //        let editVc = EditViewController()
        //        editVc.title = tasks[indexPath.row]
        //        self.navigationController?.pushViewController(editVc, animated: true)
    }
    
    deinit{
        print("View Controller deinitialised")
    }
    
    
    
}


