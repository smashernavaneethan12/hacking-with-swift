//
//  AddTaskViewController.swift
//  ToDoList
//
//  Created by Temp on 18/02/22.
//

import UIKit

class AddTaskViewController: UIViewController,UITextFieldDelegate {

    var newTask: String?
    var delegate: AddTaskDelegate?
    
    let enterTextField: UITextField = {
        let textField = UITextField()
        textField.returnKeyType = .done
        textField.keyboardType = .default
        textField.placeholder = "Enter the New Task!"
        textField.textAlignment = .center
        textField.layer.borderColor = UIColor.systemBlue.cgColor
        textField.textColor = .systemBlue
        textField.layer.borderWidth = 2
        textField.layer.cornerRadius = 10
        return textField
    }()
    
    let addButton: UIButton = {
        let button = UIButton()
        button.setTitle("ADD", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .systemBlue
        button.layer.cornerRadius = 10
        return button
    }()
    
    let cancelButton: UIButton = {
        let button = UIButton()
        button.setTitle("Cancel", for: .normal)
        button.setTitleColor(.systemRed, for: .normal)
        button.backgroundColor = .clear
        button.layer.cornerRadius = 10
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Add Task"
        view.backgroundColor = .systemBackground
        enterTextField.delegate = self
        view.addSubview(enterTextField)
        view.addSubview(addButton)
        //view.addSubview(cancelButton)
        addButton.addTarget(self, action: #selector(addButtonPressed), for: .touchUpInside)
        cancelButton.addTarget(self, action: #selector(cancelButtonPressed), for: .touchUpInside)
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelButtonPressed))
        

        
        addConstraint()
    }
    
    //MARK: - TextField Delegate functions
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        newTask = textField.text
        delegate?.addTaskDelegateReturn(task: newTask!)
        textField.resignFirstResponder()
        self.dismiss(animated: true, completion: nil)
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.text = ""
    }
    
    @objc func cancelButtonPressed(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func addButtonPressed(){
        delegate?.addTaskDelegateReturn(task: enterTextField.text!)
        enterTextField.text = ""
        self.dismiss(animated: true, completion: nil)

    }
    
    
    
    //MARK: - AutoLayout constraints
    func addConstraint(){
        
        enterTextField.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            enterTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0),
            enterTextField.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -20),
            enterTextField.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.75),
            enterTextField.heightAnchor.constraint(equalToConstant: 52)
        ])
        
        addButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            addButton.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0),
            addButton.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 50),
            addButton.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.5),
            addButton.heightAnchor.constraint(equalToConstant: 40)
        ])
        
        
//        cancelButton.translatesAutoresizingMaskIntoConstraints = false
//        NSLayoutConstraint.activate([
//            cancelButton.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0),
//            cancelButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -50),
//            //cancelButton.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.5),
//            //cancelButton.heightAnchor.constraint(equalToConstant: 40)
//        ])
        
        
    }
    
    deinit{
        print("Add task deinitialised")
    }

}
