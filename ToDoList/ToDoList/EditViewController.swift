//
//  EditViewController.swift
//  ToDoList
//
//  Created by Temp on 18/02/22.
//

import UIKit

class EditViewController: UIViewController,UITextFieldDelegate {

    let taskTextField: UITextField = {
        let textField = UITextField()
        textField.returnKeyType = .done
        textField.keyboardType = .default
        textField.textAlignment = .center
        textField.layer.borderColor = UIColor.systemBlue.cgColor
        textField.textColor = .systemBlue
//        textField.layer.borderWidth = 2
//        textField.layer.cornerRadius = 10
        return textField
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        view.addSubview(taskTextField)
        taskTextField.delegate = self
        addConstraint()
    }
    
    func addConstraint(){
        taskTextField.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            taskTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0),
            taskTextField.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -20),
            taskTextField.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.75),
            taskTextField.heightAnchor.constraint(equalToConstant: 52)
        ])
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
    
}
