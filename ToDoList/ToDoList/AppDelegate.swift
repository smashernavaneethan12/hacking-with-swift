//
//  AppDelegate.swift
//  ToDoList
//
//  Created by Temp on 18/02/22.
//

import UIKit
import Firebase

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let navAppearance = UINavigationBarAppearance()
        navAppearance.backgroundColor = .systemBackground
        UINavigationBar.appearance().standardAppearance = navAppearance
        UINavigationBar.appearance().scrollEdgeAppearance = navAppearance
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        let toDoListVc = ViewController()
        toDoListVc.title = "To-Do-List"
        let toDoListNavControll = UINavigationController(rootViewController: toDoListVc)
        window?.rootViewController = toDoListNavControll
        
        
        FirebaseApp.configure()
        return true
    }


}

