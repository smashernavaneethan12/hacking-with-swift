//
//  SideMenuTableViewCell.swift
//  Assignment1AutoLayout
//
//  Created by Temp on 17/02/22.
//

import UIKit

class SideMenuTableViewCell: UITableViewCell {
    
    let logo: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.backgroundColor = .blue
        imageView.image = UIImage(named: "spiderman")
        imageView.layer.borderColor = UIColor.white.cgColor
        imageView.layer.borderWidth = 2
        return imageView
    }()
    
    let logoLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.text = "Spider-Man"
        label.font = UIFont.boldSystemFont(ofSize: 20.0)
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.backgroundColor = .black
        //backgroundColor = .red
        contentView.addSubview(logo)
        contentView.addSubview(logoLabel)
        layoutConstraints()
    }
    //MARK: - SideMenu Cell constraints
    func layoutConstraints(){

        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.heightAnchor.constraint(equalToConstant: 75).isActive = true
        contentView.widthAnchor.constraint(equalToConstant: 400).isActive = true

        //logo constraints
        logo.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            logo.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            logo.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.75),
            logo.widthAnchor.constraint(equalTo: logo.heightAnchor),
            logo.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10)
        ])
        logo.layoutIfNeeded()
        logo.layer.cornerRadius = logo.frame.size.height/2.0
        
        //logoLabel constraints
        logoLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            logoLabel.heightAnchor.constraint(equalToConstant: 25),
            logoLabel.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 0.75),
            logoLabel.centerYAnchor.constraint(equalTo: logo.centerYAnchor),
            logoLabel.leadingAnchor.constraint(equalTo: logo.trailingAnchor, constant: 15)
        ])
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
