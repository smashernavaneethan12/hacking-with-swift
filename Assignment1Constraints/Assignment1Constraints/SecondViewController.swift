//
//  SecondViewController.swift
//  Assignment1AutoLayout
//
//  Created by Temp on 16/02/22.
//

import UIKit
import SnapKit
import SideMenu

class SecondViewController: UIViewController, UITextFieldDelegate {
    
    //MARK: - all properties
    let data = ImageData()
    var myImage: String = "spiderman"
    var potraitConstraints: [NSLayoutConstraint]?
    var landscapeConstraints: [NSLayoutConstraint]?
    var menuView: SideMenuNavigationController?
    
    
    
    private let topView: UIView = {
        let view = UIView()
        //view.backgroundColor = .green
        return view
    }()
    
    private let bottomView: UIView = {
        let view = UIView()
        //view.backgroundColor = .blue
        return view
    }()
    
    private let squareView: UIView = {
        let view = UIView()
        view.backgroundColor = .yellow
        view.layer.borderWidth = 2
        view.layer.borderColor = UIColor.link.cgColor
        return view
    }()
    
    private let textLabel: UILabel = {
        let label = UILabel()
        label.text = "Text Label"
        label.textColor = .black
        label.backgroundColor = .white
        label.layer.borderColor = UIColor.red.cgColor
        label.layer.borderWidth = 2
        label.textAlignment = .center
        return label
    }()
    
    private let circleView: UIView = {
        let view = UIView()
        view.backgroundColor = .systemBlue
        view.layer.borderWidth = 2
        view.layer.borderColor = UIColor.red.cgColor
        view.clipsToBounds = true
        
        return view
    }()
    
    private let blackButton: UIButton = {
        let button = UIButton()
        button.setTitle("BLACK", for: .normal)
        button.backgroundColor = .black
        button.setTitleColor(.white, for: .normal)
        //button.addTarget(self, action: #selector(blackButtonPressed), for: .touchUpInside)
        return button
    }()
    
    private let redButton: UIButton = {
        let button = UIButton()
        button.setTitle("RED", for: .normal)
        button.backgroundColor = .black
        button.setTitleColor(.white, for: .normal)
        return button
    }()
    
    private let textField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Enter Value"
        textField.textAlignment = .center
        textField.layer.borderColor = UIColor.black.cgColor
        textField.layer.borderWidth = 2
        textField.textColor = .black
        textField.keyboardType = UIKeyboardType.default
        textField.returnKeyType = UIReturnKeyType.done
        return textField
    }()
    
    private let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.borderWidth = 2
        imageView.layer.borderColor = UIColor.green.cgColor
        imageView.backgroundColor = .lightGray
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private let loadImg: UIButton = {
        let button = UIButton()
        button.backgroundColor = .black
        button.setTitle("LOAD IMAGE", for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        return button
    }()
    
    private let toggleSwitch: UISwitch = {
        let iSwitch = UISwitch()
        iSwitch.isOn = true
        return iSwitch
    }()
    
    //MARK: - view life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .systemTeal
        textField.delegate = self
        title = "HOME"
        menuView = SideMenuNavigationController(rootViewController: SideMenuTableViewController())
        menuView?.leftSide = true
        menuView?.setNavigationBarHidden(true, animated: true)
        SideMenuManager.default.leftMenuNavigationController = menuView
        SideMenuManager.default.addPanGestureToPresent(toView: self.view)
        
        
        // adding subViews
        view.addSubview(bottomView)
        view.addSubview(topView)
        
        topView.addSubview(textLabel)
        topView.addSubview(squareView)
        topView.addSubview(circleView)
        topView.addSubview(blackButton)
        topView.addSubview(redButton)
        topView.addSubview(textField)
        
        bottomView.addSubview(imageView)
        bottomView.addSubview(loadImg)
        bottomView.addSubview(toggleSwitch)
        
        blackButton.addTarget(self, action: #selector(self.blackButtonPressed), for: .touchUpInside)
        redButton.addTarget(self, action: #selector(self.redButtonPressed), for: .touchUpInside)
        loadImg.addTarget(self, action: #selector(loadImgPressed), for: .touchUpInside)
        toggleSwitch.addTarget(self, action: #selector(toggleSwitchToggled), for: .valueChanged)
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "house"), style: .done, target: self, action: #selector(homePressed))
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "location.circle.fill"), style: .done, target: self, action: #selector(mapPressed))
        
    }
    
    
    override func viewDidLayoutSubviews() {
        setupAutoLayout()
    }
    
    //MARK: - Bar Buttons Actions
    @objc func homePressed(){
        print("Home pressed")
        present(menuView!, animated: true, completion: nil)
    }
    
    @objc func mapPressed(){
        navigationController?.pushViewController(MapViewController(), animated: true)
    }
    //MARK: - AutoLayout constraints
    // Function for auto layoutlo
    private func setupAutoLayout(){
        
        //topView constraints
        topView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
                        topView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
                        topView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.5),
                        topView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
                        topView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor)
                    ])
        topView.layoutIfNeeded()
        
        // bottom view constraints
         bottomView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            bottomView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            bottomView.topAnchor.constraint(equalTo: topView.bottomAnchor),
            bottomView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            bottomView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor)
        ])
        
        //squareView constraints
        squareView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            squareView.heightAnchor.constraint(equalTo: topView.heightAnchor, multiplier: 0.20),
            squareView.widthAnchor.constraint(equalTo: squareView.heightAnchor),
            squareView.centerXAnchor.constraint(equalTo: topView.centerXAnchor),
            squareView.topAnchor.constraint(equalTo: topView.topAnchor, constant: 20)
        ])
        
        // circleView constraints
        circleView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            circleView.heightAnchor.constraint(equalTo: topView.heightAnchor, multiplier: 0.20),
            circleView.widthAnchor.constraint(equalTo: circleView.heightAnchor),
            circleView.centerXAnchor.constraint(equalTo: topView.centerXAnchor),
            circleView.topAnchor.constraint(equalTo: squareView.bottomAnchor, constant: 20)
        ])

        //circleView.setNeedsLayout()
//        circleView.layoutSubviews()
        circleView.layoutIfNeeded()
//        let circleHeight = circleView.frame.height
        circleView.layer.cornerRadius = circleView.frame.height/2.0
//        print("auto \(circleView.frame.height)")
        
        //text label constraints
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            textLabel.heightAnchor.constraint(equalTo: topView.heightAnchor, multiplier: 0.1),
            textLabel.widthAnchor.constraint(equalTo: topView.widthAnchor, multiplier: 0.33),
            textLabel.topAnchor.constraint(equalTo: circleView.bottomAnchor, constant: 10),
            textLabel.leadingAnchor.constraint(equalTo: topView.leadingAnchor, constant: 5)
        ])
        
        //blackButton constraints
        blackButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            blackButton.heightAnchor.constraint(equalTo: topView.heightAnchor, multiplier: 0.1),
            blackButton.leadingAnchor.constraint(equalTo: textLabel.trailingAnchor, constant: 5),
            blackButton.widthAnchor.constraint(equalTo: topView.widthAnchor, multiplier: 0.33),
            blackButton.topAnchor.constraint(equalTo: circleView.bottomAnchor, constant: 10)
        ])
        
        //redButton constraints
        redButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            redButton.heightAnchor.constraint(equalTo: topView.heightAnchor, multiplier: 0.1),
            redButton.leadingAnchor.constraint(equalTo: blackButton.trailingAnchor, constant: 5),
            redButton.topAnchor.constraint(equalTo: circleView.bottomAnchor, constant: 10),
            redButton.trailingAnchor.constraint(equalTo: topView.trailingAnchor, constant: -5)
        ])
        
        //textField constraints
        textField.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            textField.centerXAnchor.constraint(equalTo: topView.centerXAnchor),
            textField.heightAnchor.constraint(equalTo: topView.heightAnchor, multiplier: 0.1),
            textField.widthAnchor.constraint(equalTo: topView.widthAnchor, multiplier: 0.5),
            textField.topAnchor.constraint(equalTo: blackButton.bottomAnchor, constant: 10)
        ])
        
        //imageView constraints
        imageView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            imageView.heightAnchor.constraint(equalTo: bottomView.heightAnchor, multiplier: 0.5),
            imageView.widthAnchor.constraint(equalTo: imageView.heightAnchor),
            imageView.topAnchor.constraint(equalTo: bottomView.topAnchor, constant: 10),
            imageView.leadingAnchor.constraint(equalTo: topView.leadingAnchor, constant: 10)
        ])
        
        //loadImg constraints
        loadImg.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            loadImg.heightAnchor.constraint(equalTo: bottomView.heightAnchor, multiplier: 0.1),
            loadImg.trailingAnchor.constraint(equalTo: bottomView.trailingAnchor, constant: -10),
            loadImg.leadingAnchor.constraint(equalTo: imageView.trailingAnchor, constant: 10),
            loadImg.centerYAnchor.constraint(equalTo: imageView.centerYAnchor)
        ])
        
        //toggleSwitch constraints
        toggleSwitch.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            toggleSwitch.centerXAnchor.constraint(equalTo: imageView.centerXAnchor),
            toggleSwitch.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 15)
        ])
    }
    
    //MARK: - Button Actions
    @objc func blackButtonPressed(sender: UIButton){
        textLabel.textColor = .black
    }
    
    @objc func redButtonPressed(sender: UIButton){
        textLabel.textColor = .red
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textLabel.text = textField.text
        textField.resignFirstResponder()
        return true
    }
    
    @objc func loadImgPressed(sender: UIButton){
        myImage = data.pickImage()
        if toggleSwitch.isOn{
            imageView.image = UIImage(named: myImage)
        }else{
            imageView.image = UIImage(named: myImage+"Black")
        }
    }
    
    @objc func toggleSwitchToggled(sender: UISwitch){
        
        if sender.isOn{
            imageView.image = UIImage(named: myImage)
        }else{
            imageView.image = UIImage(named: myImage+"Black")
        }
    }
}
