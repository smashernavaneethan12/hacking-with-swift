//
//  AppDelegate.swift
//  Assignment1Constraints
//
//  Created by Temp on 18/02/22.
//

import UIKit
import SideMenu

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let entryVc = SecondViewController()
    var entryNav: UINavigationController?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
                
        //navigationBar appearance
        let navAppearance = UINavigationBarAppearance()
        navAppearance.configureWithOpaqueBackground()
        navAppearance.backgroundColor = .systemBackground
        UINavigationBar.appearance().standardAppearance = navAppearance
        UINavigationBar.appearance().scrollEdgeAppearance = navAppearance
        
        //tabBar appearance
        let tabAppearance = UITabBarAppearance()
        tabAppearance.configureWithOpaqueBackground()
        tabAppearance.backgroundColor = .systemBackground
        UITabBar.appearance().standardAppearance = tabAppearance
        UITabBar.appearance().scrollEdgeAppearance = tabAppearance
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        let oneNav = UINavigationController(rootViewController: entryVc)
        let twoVc = MapViewController()
        twoVc.view.backgroundColor = .systemMint
        twoVc.title = "MAP"
        entryVc.tabBarItem.image = UIImage(systemName: "house")
        twoVc.tabBarItem.image = UIImage(systemName: "location.circle.fill")
        let twoNav = UINavigationController(rootViewController: twoVc)
        twoVc.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .camera, target: self, action: nil)
        
        let entryTab = UITabBarController()
        entryTab.setViewControllers([oneNav,twoNav], animated: true)
        window?.rootViewController = entryTab
        
        return true
    }
    
   
}
