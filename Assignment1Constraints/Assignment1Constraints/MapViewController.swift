//
//  MapViewController.swift
//  Assignment1AutoLayout
//
//  Created by Temp on 17/02/22.
//

import UIKit
import MapKit

class MapViewController: UIViewController {
    
    let mapView: MKMapView = {
        let map = MKMapView()
        return map
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "MAP"
        view.backgroundColor = .systemBackground
        view.addSubview(mapView)
        
        setViewConstraints()
    }
    override func viewDidLayoutSubviews() {
        mapView.frame = view.bounds
    }
    
    func setViewConstraints(){
       
        //mapView constraints
        mapView.translatesAutoresizingMaskIntoConstraints = false
        
    }
}
