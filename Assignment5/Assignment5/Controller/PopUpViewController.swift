//
//  PopUpViewController.swift
//  Assignment5
//
//  Created by Temp on 05/04/22.
//

import UIKit
import SnapKit


protocol PopUpViewControllerDelegate{
    func didSelectApplyButton(controller: PopUpViewController, present vc: AuthenticationViewController)
}

class PopUpViewController: UIViewController {
    
    var delegate: PopUpViewControllerDelegate?

    let topDragger: UIView = {
        let dragger = UIView()
        dragger.backgroundColor = .black
        dragger.alpha = 0.3
        return dragger
    }()
    
    let authTableView = UITableView()
    
   
    let authView1 = AuthTileView(model: AuthModel(title: "None", description: "No Authentication, user can sign without any kind of authentication.", image: UIImage(named: "thumbsup")!))
    let authView2 = AuthTileView(model: AuthModel(title: "Email", description: "Email authentication provides a way to verify that an email comes from who it claims to be from.", image: UIImage(named: "email")!))
    let authView3 = AuthTileView(model: AuthModel(title: "Offline", description: "Sign Documents even when you're offline and save them as drafts.", image: UIImage(named: "offline")!))
    let authView4 = AuthTileView(model: AuthModel(title: "SMS", description: "Provides a code that has been sent to your phone via SMS as proof.", image: UIImage(named: "sms")!))
//    let authView5 = AuthTileView(model: AuthModel(title: "SMS", description: "Provides a code that has been sent to your phone via SMS as proof.", image: UIImage(systemName: "house")!))
    
    
    lazy var popUpItems: [AuthTileView] = {
        
        let items: [AuthTileView] = [authView1,authView2,authView3,authView4]
        return items
        
    }()
    
    let authStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = 20
        stack.distribution = .fillEqually
        return stack
    }()
    
    lazy var bottomButton: UIButton = {
        let button = UIButton()
        button.setTitle("Apply Authentication", for: .normal)
        button.backgroundColor = ZColors.greenColor
        button.addTarget(self, action: #selector(bottomButtonPressed), for: .touchUpInside)
        return button
    }()
    
    var selectedCount: Int = 0
    
    var authTileCells: [AuthTileCell] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .systemBackground
       
        authTableView.register(AuthTileCell.self, forCellReuseIdentifier: AuthTileCell.reuseIdentifier)
        authTableView.delegate = self
        authTableView.dataSource = self
        authTableView.separatorStyle = .none
        
        self.navigationItem.title = "Authentication"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "arrow.backward"), style: .plain, target: self, action: #selector(backButton))
        self.navigationItem.leftBarButtonItem?.tintColor = .black
        
        setUpDragger()
    }
    

    @objc func backButton(){
        self.dismiss(animated: true)
    }
   
    @objc func bottomButtonPressed(_ sender: UIButton){
//        print((sender.titleLabel?.text)!)
        var model: AuthModel?
        var flag: Int = 0
        authTileCells.forEach { authCell in
            if authCell.tileIsSelected{
                flag = 1
                model = authCell.model
            }
        }
        
        if flag == 0{
            
            let alert = UIAlertController(title: "No Selection", message: "Select any one of the authentication method", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default))
            present(alert, animated: true)
            
        }else if flag == 1{
            let vc = AuthenticationViewController()
            vc.model = model
//            let nav = UINavigationController(rootViewController: vc)
            delegate?.didSelectApplyButton(controller: self, present: vc)
//            present(nav, animated: true)
        }
        
    }

    func setUpDragger(){
        view.addSubview(topDragger)
        topDragger.translatesAutoresizingMaskIntoConstraints = false
        topDragger.layer.cornerRadius = 3
        NSLayoutConstraint.activate([
            topDragger.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            topDragger.topAnchor.constraint(equalTo: view.topAnchor, constant: 5),
            topDragger.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.1),
            topDragger.heightAnchor.constraint(equalToConstant: 5)
        ])
        
//        view.addSubview(authStack)
        view.addSubview(authTableView)
        view.addSubview(bottomButton)
        
//        for item in popUpItems{
//            authStack.addArrangedSubview(item)
//            item.layer.cornerRadius = 25
//        }
        
//        authStack.snp.makeConstraints { make in
//            make.top.equalTo(view.safeAreaLayoutGuide)
//            make.leading.equalTo(view.safeAreaLayoutGuide).offset(15)
//            make.trailing.equalTo(view.safeAreaLayoutGuide).offset(-15)
//            make.bottom.equalTo(bottomButton.snp.top).offset(-30)
//        }
        
        authTableView.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide)
            make.leading.equalTo(view.safeAreaLayoutGuide).offset(15)
            make.trailing.equalTo(view.safeAreaLayoutGuide).offset(-15)
            make.bottom.equalTo(bottomButton.snp.top).offset(-30)
        }
        
        bottomButton.snp.makeConstraints { make in
            make.bottom.equalToSuperview().offset(-20)
            make.centerX.equalToSuperview()
            make.height.equalTo(52)
            make.width.equalToSuperview().multipliedBy(0.75)
            
        }
        bottomButton.layoutIfNeeded()
        bottomButton.layer.cornerRadius = bottomButton.frame.height / 2
        
        
    }
}

extension PopUpViewController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return popUpItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = AuthTileCell(style: .default, reuseIdentifier: AuthTileCell.reuseIdentifier, model: popUpItems[indexPath.row].model!)
        cell.layer.cornerRadius = 25
        cell.delegate = self
        cell.selectionStyle = .none
        cell.tileImage.layoutIfNeeded()
        
        cell.tileImage.snp.makeConstraints { make in
            make.width.equalTo(cell.tileImage.snp.height)
        }
        
        authTileCells.append(cell)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let cell = cell as? AuthTileCell else { return }

        cell.layoutIfNeeded()
        print(cell.tileImage.frame, cell.tileBackgroundView.frame)
        cell.tileImage.snp.updateConstraints { make in
            make.height.equalTo(cell.tileBackgroundView.snp.height).multipliedBy(0.4)
            make.width.equalTo(cell.tileImage.snp.height)
        }
        
        print(cell.tileImage.frame,  cell.tileBackgroundView.frame)
        cell.tileImage.layer.cornerRadius = cell.tileImage.frame.width / 2.0
        cell.tileImage.layer.borderColor = UIColor.green.cgColor
        cell.tileImage.layer.borderWidth = 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(tableView.frame.height / CGFloat( popUpItems.count))
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)

//        guard let tile = tableView.cellForRow(at: indexPath) as? AuthTileCell else { return }
//        if !tile.tileIsSelected{
//            tile.selectedView.isHidden = false
//            tile.tileImage.image = UIImage(named: "ok-1976099")
//            tile.tileIsSelected.toggle()
//        }else{
//            tile.selectedView.isHidden = true
//            tile.tileImage.image = tile.model?.image
//            tile.tileIsSelected.toggle()
//        }
        
        guard let cell = tableView.cellForRow(at: indexPath) as? AuthTileCell else { return }
        
        if !cell.tileIsSelected{
            cell.selectedView.isHidden = false
            cell.tileImage.image = UIImage(named: "ok-1976099")
            cell.tileIsSelected.toggle()
            delSelectRemainingCells(except: cell)
        }else{
            cell.selectedView.isHidden = true
            cell.tileImage.image = cell.model?.image
            cell.tileIsSelected.toggle()
        }
        print("\n")
        for authcell in authTileCells{
            print("\(authcell.tileTitle.text!) tile selection ---- \(authcell.tileIsSelected)")
        }
        
    }
    
}

extension PopUpViewController: AuthTileSelectionManagerDelegate{
    func didSelectTile(at cell: AuthTileCell) {
    
//        // if no tile is selected
//        if selectedCount == 0{
//            if !cell.tileIsSelected{
//                cell.selectedView.isHidden = false
//                cell.tileImage.image = UIImage(named: "ok-1976099")
//                cell.tileIsSelected.toggle()
//                selectedCount += 1
//            }else{
//                cell.selectedView.isHidden = true
//                cell.tileImage.image = cell.model?.image
//                cell.tileIsSelected.toggle()
//                selectedCount -= 1
//            }
//        }else if selectedCount == 1{ // one tile is selected that tile can only be deselected
//
//            if cell.tileIsSelected{
//                cell.selectedView.isHidden = true
//                cell.tileImage.image = cell.model?.image
//                cell.tileIsSelected.toggle()
//                selectedCount -= 1
//            }else{
//                // only deselection of the selected tile is possible
//            }
//        }
        if !cell.tileIsSelected{
            cell.selectedView.isHidden = false
            cell.tileImage.image = UIImage(named: "ok-1976099")
            cell.tileIsSelected.toggle()
            delSelectRemainingCells(except: cell)
        }else{
            cell.selectedView.isHidden = true
            cell.tileImage.image = cell.model?.image
            cell.tileIsSelected.toggle()
        }
        print("\n")
        for authcell in authTileCells{
            print("\(authcell.tileTitle.text!) tile selection ---- \(authcell.tileIsSelected)")
        }
    }
    
    func delSelectRemainingCells(except cell: AuthTileCell){
        for authCell in authTileCells{
            if authCell == cell{
                
            }else{
                if authCell.tileIsSelected{
                    authCell.selectedView.isHidden = true
                    authCell.tileImage.image = authCell.model?.image
                    authCell.tileIsSelected.toggle()
                }
            }
        }
    }
    
    
}
