//
//  AuthenticationViewController.swift
//  Assignment5
//
//  Created by Temp on 06/04/22.
//

import UIKit

class AuthenticationViewController: UIViewController {
    
    var model: AuthModel?
    
    let label: UILabel = {
        let label = UILabel(frame: CGRect(origin: .zero, size: CGSize(width: 200, height: 52)))
        label.font = UIFont.boldSystemFont(ofSize: 15)
        label.textColor = .black
        label.numberOfLines = 0
        return label
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .systemBackground
        self.navigationItem.title = "Authentication Details"
        
        view.addSubview(label)
        label.text = "Selected mode is \(model!.title)"
        label.center = view.center
        
    }
    
    

}
