//
//  CustomPresentationController.swift
//  Assignment5
//
//  Created by Temp on 05/04/22.
//

import Foundation
import UIKit

class CustomPresentationController: UIPresentationController {
    
    lazy var dimmingView: UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.frame = CGRect(x: 0, y: 0, width: self.containerView!.frame.width, height: self.containerView!.frame.height)
        view.alpha = 0.4
        return view
    }()
    
    var popUpHeight: CGFloat = 700
    
    var buttonHeight: CGFloat = 52
    
    override init(presentedViewController: UIViewController, presenting presentingViewController: UIViewController?) {
        super.init(presentedViewController: presentedViewController, presenting: presentingViewController)
        if presentedViewController is UINavigationController{
          
            let vc = (presentedViewController as! UINavigationController).viewControllers.first
            
            if let popVc = vc as? PopUpViewController{
                popUpHeight = (CGFloat(popVc.popUpItems.count) * CGFloat(140.0)) + buttonHeight + CGFloat(50.0)
            }
        }
    }
    
    override var presentedView: UIView {
        return presentedViewController.view
    }
    
    override var frameOfPresentedViewInContainerView: CGRect {
        
        
        return CGRect(x: 0, y: containerView!.frame.height - popUpHeight, width: containerView!.frame.width, height: popUpHeight)
        
    }
    
    override func containerViewDidLayoutSubviews() {
        super.containerViewDidLayoutSubviews()

        containerView?.insertSubview(dimmingView, at: 0)
        presentedView.frame = frameOfPresentedViewInContainerView
        containerView?.addSubview(presentedView)
        
    }
    
    
}
