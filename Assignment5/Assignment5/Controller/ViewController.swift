//
//  ViewController.swift
//  Assignment5
//
//  Created by Temp on 05/04/22.
//

import UIKit
import SnapKit

class ViewController: UIViewController {

    lazy var popUpButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = ZColors.greenColor
        button.setTitle("Open Pop Up", for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.addTarget(self, action: #selector(showPopUpView), for: .touchUpInside)
        return button
    }()
    
    
    let backgroundView: UIImageView = {
        let view = UIImageView()
        view.backgroundColor = .green
        view.isUserInteractionEnabled = true
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        
        
        view.addSubview(backgroundView)
        backgroundView.addSubview(popUpButton)
        
        backgroundView.image = UIImage(named: "5660740")
        
        let layer0 = CAGradientLayer()
        layer0.colors = [
          UIColor(red: 1, green: 1, blue: 1, alpha: 1).cgColor,
          UIColor(red: 1, green: 1, blue: 1, alpha: 0).cgColor
        ]
        layer0.locations = [0, 1]
        layer0.startPoint = CGPoint(x: 0.25, y: 0.5)
        layer0.endPoint = CGPoint(x: 0.75, y: 0.5)
        layer0.transform = CATransform3DMakeAffineTransform(CGAffineTransform(a: 0, b: 1, c: -1, d: 0, tx: 1, ty: 0))
        layer0.bounds = view.bounds.insetBy(dx: -0.5*view.bounds.size.width, dy: -0.5*view.bounds.size.height)
        layer0.position = view.center
        
        view.layer.addSublayer(layer0)
        
       
       snapKitLayoutViews()
        
    }

    
//MARK: - Layout Views
    
    func snapKitLayoutViews(){
        
        backgroundView.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.height.equalTo(80)
            make.width.equalToSuperview().multipliedBy(0.7)
        }
        backgroundView.layoutIfNeeded()
        backgroundView.layer.cornerRadius = popUpButton.frame.height / 2
        
        popUpButton.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.height.equalTo(52)
            make.width.equalToSuperview().multipliedBy(0.6)
            
        }
        popUpButton.layoutIfNeeded()
        popUpButton.layer.cornerRadius = popUpButton.frame.height / 2
    }
    
    
//MARK: - Button Actions
    
    @objc func showPopUpView(_ sender: UIButton){
        
        let vc = PopUpViewController()
        vc.delegate = self
        let nav = UINavigationController(rootViewController: vc)
        nav.modalPresentationStyle = .custom
        nav.transitioningDelegate = self
        present(nav, animated: true)
        
    }

    
}


extension ViewController: UIViewControllerTransitioningDelegate{
    
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        
        let presentationController = CustomPresentationController(presentedViewController: presented, presenting: presenting)
        return presentationController
    }
}

extension ViewController: PopUpViewControllerDelegate{
    func didSelectApplyButton(controller: PopUpViewController, present vc: AuthenticationViewController) {
        controller.dismiss(animated: true)
        
        let nav = UINavigationController(rootViewController: vc)
        self.present(nav, animated: true)
    }
}
