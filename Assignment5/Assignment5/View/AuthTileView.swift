//
//  AuthTileView.swift
//  Assignment5
//
//  Created by Temp on 05/04/22.
//

import Foundation
import UIKit
import SnapKit

class AuthTileView: UIView{
    
    let tileTitle: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = UIFont.boldSystemFont(ofSize: 15)
        label.numberOfLines = 0
        return label
    }()
    
    let tileDescription: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 15)
        label.numberOfLines = 0
        return label
    }()

    let tileImage: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFit
        image.clipsToBounds = true
        return image
    }()
    
    let selectedView: UIView = {
        let view = UIView()
        view.backgroundColor = ZColors.greenColor
        view.alpha = 0.25
        view.layer.borderColor = UIColor.green.cgColor
        view.layer.borderWidth = 2
        view.isHidden = true
        return view
    }()
    
    var isSelected: Bool = false
    
    lazy var tapGesture: UITapGestureRecognizer = {
        let gesture = UITapGestureRecognizer()
        gesture.addTarget(self, action: #selector(tapGestureTriggerd))
        gesture.numberOfTapsRequired = 1
        gesture.numberOfTouchesRequired = 1
        return gesture
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
//        snapKitLayoutViews()
    }
    
    convenience init(model: AuthModel){
        self.init()
        self.model = model
        self.addGestureRecognizer(tapGesture)
        snapKitLayoutViews()
    }
    
    var model: AuthModel?
    
    func snapKitLayoutViews(){
        backgroundColor = ZColors.lightSkinColor
        addSubview(tileImage)
        addSubview(tileTitle)
        addSubview(tileDescription)
        addSubview(selectedView)
        
        
//        tileImage.translatesAutoresizingMaskIntoConstraints = false
        tileImage.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.equalToSuperview().offset(5)
            make.height.equalToSuperview().multipliedBy(0.5)
            make.width.equalTo(tileImage.snp.height)
        }
        self.layoutSubviews()
        self.layoutIfNeeded()
        tileImage.layoutIfNeeded()
//        tileImage.backgroundColor = .red
        tileImage.layer.cornerRadius = tileImage.frame.height / 2
//        tileImage.layer.borderColor = UIColor.green.cgColor
//        tileImage.layer.borderWidth = 2
        
        tileTitle.snp.makeConstraints { make in
            make.leading.equalTo(tileImage.snp.trailing).offset(5)
            make.top.equalToSuperview().offset(15)
//            make.height.equalTo(30)
            make.trailing.equalToSuperview()
        }
        
        tileDescription.snp.makeConstraints { make in
            make.top.equalTo(tileTitle.snp.bottom).offset(5)
            make.leading.equalTo(tileImage.snp.trailing).offset(5)
//            make.height.equalTo(30)
            make.trailing.equalToSuperview()
        }
        
        selectedView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        selectedView.layoutIfNeeded()
        selectedView.layer.cornerRadius = 25
        
        tileTitle.text = model?.title
        tileDescription.text = model?.description
        tileImage.image = model?.image
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func tapGestureTriggerd(_ gesture: UITapGestureRecognizer){
        
        if !isSelected{
            selectedView.isHidden = false
            tileImage.image = UIImage(named: "ok-1976099")
            isSelected.toggle()
        }else{
            selectedView.isHidden = true
            tileImage.image = model?.image
            isSelected.toggle()
        }
        
    }
    
}
