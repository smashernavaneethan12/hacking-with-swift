//
//  AuthTileCell.swift
//  Assignment5
//
//  Created by Temp on 06/04/22.
//

import UIKit

class AuthTileCell: UITableViewCell {
    
    static let reuseIdentifier = "AuthCellReuseId"
    
    let tileTitle: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = UIFont.boldSystemFont(ofSize: 15)
        label.numberOfLines = 0
        return label
    }()
    
    let tileDescription: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 15)
        label.numberOfLines = 0
        return label
    }()

   public let tileImage: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFit
        image.clipsToBounds = true
        return image
    }()
    
    let selectedView: UIView = {
        let view = UIView()
        view.backgroundColor = ZColors.greenColor
        view.alpha = 0.25
        view.layer.borderColor = UIColor.green.cgColor
        view.layer.borderWidth = 2
        view.isHidden = true
        return view
    }()
    
    public let tileBackgroundView: UIView = {
        let view = UIView()
        view.backgroundColor = ZColors.lightSkinColor
        return view
    }()
    
    var tileIsSelected: Bool = false
    
    var delegate: AuthTileSelectionManagerDelegate?
    
    lazy var tapGesture: UITapGestureRecognizer = {
        let gesture = UITapGestureRecognizer()
        gesture.addTarget(self, action: #selector(tapGestureTriggerd))
        gesture.numberOfTapsRequired = 1
        gesture.numberOfTouchesRequired = 1
        return gesture
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        
    }
    convenience init(style: UITableViewCell.CellStyle, reuseIdentifier: String?, model: AuthModel){
        self.init(style: style, reuseIdentifier: reuseIdentifier)
        self.model = model
//        self.addGestureRecognizer(tapGesture)
//        self.isUserInteractionEnabled = true
        contentView.isUserInteractionEnabled = false
        snapKitLayoutViews()
    }
    
    var model: AuthModel?
    
    func snapKitLayoutViews(){
//        backgroundColor = ZColors.lightSkinColor
        
        addSubview(tileBackgroundView)
        tileBackgroundView.addSubview(tileImage)
        tileBackgroundView.addSubview(tileTitle)
        tileBackgroundView.addSubview(tileDescription)
        tileBackgroundView.addSubview(selectedView)
        
        
        tileBackgroundView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(7)
            make.bottom.equalToSuperview().offset(-7)
            make.leading.trailing.equalToSuperview()
        }
        
        tileBackgroundView.layer.cornerRadius = 25
        tileBackgroundView.layoutIfNeeded()
//        tileImage.translatesAutoresizingMaskIntoConstraints = false
        tileImage.snp.makeConstraints { make in
            make.centerY.equalTo(tileBackgroundView)
            make.leading.equalTo(tileBackgroundView).offset(5)
//            make.height.width.equalTo(40)
            make.height.equalTo(tileBackgroundView.snp.height).multipliedBy(0.4)
            make.width.equalTo(tileImage.snp.height)
        }
        
//        self.layoutSubviews()
//        self.layoutIfNeeded()
        tileImage.layoutIfNeeded()
//        tileImage.backgroundColor = .red
//        tileImage.layer.cornerRadius = tileImage.frame.height / 2
//        tileImage.layer.borderColor = UIColor.green.cgColor
//        tileImage.layer.borderWidth = 2
        
        tileTitle.snp.makeConstraints { make in
            make.leading.equalTo(tileImage.snp.trailing).offset(5)
            make.top.equalTo(tileBackgroundView).offset(15)
//            make.height.equalTo(30)
            make.trailing.equalTo(tileBackgroundView)
        }
        
        tileDescription.snp.makeConstraints { make in
            make.top.equalTo(tileTitle.snp.bottom).offset(5)
            make.leading.equalTo(tileImage.snp.trailing).offset(5)
//            make.height.equalTo(30)
            make.trailing.equalTo(tileBackgroundView)
        }
        
        selectedView.snp.makeConstraints { make in
            make.edges.equalTo(tileBackgroundView)
        }
        selectedView.layoutIfNeeded()
        selectedView.layer.cornerRadius = 25
        
        tileTitle.text = model?.title
        tileDescription.text = model?.description
        tileImage.image = model?.image
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func tapGestureTriggerd(_ gesture: UITapGestureRecognizer){
        
        delegate?.didSelectTile(at: self)
//        if !tileIsSelected{
//            selectedView.isHidden = false
//            tileImage.image = UIImage(named: "ok-1976099")
//            tileIsSelected.toggle()
//        }else{
//            selectedView.isHidden = true
//            tileImage.image = model?.image
//            tileIsSelected.toggle()
//        }
        
    }
    
    
}
