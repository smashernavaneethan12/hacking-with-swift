//
//  ZColors.swift
//  Assignment5
//
//  Created by Temp on 05/04/22.
//

import Foundation
import UIKit

struct ZColors{
    static var greenColor = UIColor(red: 42/255, green: 170/255, blue: 138/255, alpha: 0.9)
    static var lightSkinColor = UIColor(red: 247/255, green: 249/255, blue: 246/255, alpha: 1)
}
