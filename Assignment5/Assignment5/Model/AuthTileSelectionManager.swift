//
//  AuthTileSelectionManager.swift
//  Assignment5
//
//  Created by Temp on 06/04/22.
//

import Foundation
import UIKit

protocol AuthTileSelectionManagerDelegate{
    func didSelectTile(at cell: AuthTileCell)
}

class AuthTileSelectionManager{
    var delegate: AuthTileSelectionManagerDelegate?
    
    
}
