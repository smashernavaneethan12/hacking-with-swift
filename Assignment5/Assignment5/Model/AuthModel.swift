//
//  AuthModel.swift
//  Assignment5
//
//  Created by Temp on 05/04/22.
//

import Foundation
import UIKit

struct AuthModel{
    var title: String
    var description: String
    var image: UIImage
}
