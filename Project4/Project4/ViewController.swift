//
//  ViewController.swift
//  Project4
//
//  Created by Temp on 23/02/22.
//

import UIKit
import WebKit

class ViewController: UIViewController, WKNavigationDelegate {

    lazy var webView: WKWebView = {
        
        let config = WKWebViewConfiguration()
        WKWebsiteDataStore.nonPersistent()
        let view = WKWebView(frame: .zero, configuration: config)
    
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(webView)
        webView.load(URLRequest(url: URL(string: "https://www.youtube.com")!))
        webView.allowsBackForwardNavigationGestures = true
        webView.navigationDelegate = self
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .compose, target: self, action: #selector(selectPages))
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .undo, target: self, action: #selector(goToPreviousPage))
        
        webView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            webView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            webView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            webView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            webView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor)
        ])
    }

    override func loadView() {
        super.loadView()
        if webView.canGoBack{
            navigationItem.leftBarButtonItem?.isEnabled = true
        }else{
            navigationItem.leftBarButtonItem?.isEnabled = false
        }
    }
    @objc func goToPreviousPage(){
        webView.goBack()
    }
    @objc func refreshWebView(){
        webView.reload()
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        title = webView.title
    }

    @objc func selectPages(){
        let actionSheet = UIAlertController(title: "Pages", message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "google.com", style: .default, handler: { action in
            guard let title = action.title else { return }
            guard let url = URL(string: "https://"+title) else { return }
            self.webView.load(URLRequest(url: url))
        }))
        actionSheet.addAction(UIAlertAction(title: "youtube.com", style: .default, handler: { action in
            guard let title = action.title else { return }
            guard let url = URL(string: "https://"+title) else { return }
            self.webView.load(URLRequest(url: url))
        }))
        actionSheet.addAction(UIAlertAction(title: "instagram.com", style: .default, handler: { action in
            guard let title = action.title else { return }
            guard let url = URL(string: "https://"+title) else { return }
            self.webView.load(URLRequest(url: url))
        }))
        actionSheet.addAction(UIAlertAction(title: "cancel", style: .cancel, handler: nil))
        
        present(actionSheet, animated: true)
    }
}

