//
//  ViewController.swift
//  Assignment4_CustomTableViewCell
//
//  Created by Temp on 22/02/22.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    let mainTabel = UITableView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Table"
        view.backgroundColor = .white
        mainTabel.delegate = self
        mainTabel.dataSource = self
        mainTabel.backgroundColor = .white
        mainTabel.register(CustomTableViewCell.self, forCellReuseIdentifier: "customCells")
        view.addSubview(mainTabel)
        addConstraint()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "customCells", for: indexPath) as? CustomTableViewCell{
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "customCells", for: indexPath)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        mainTabel.deselectRow(at: indexPath, animated: true)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return view.frame.height * 0.15
//        return 70
    }
    func addConstraint(){
        mainTabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            mainTabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            mainTabel.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            mainTabel.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            mainTabel.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor)
        ])

    }

}

