//
//  CustomTableViewCell.swift
//  Assignment4_CustomTableViewCell
//
//  Created by Temp on 22/02/22.
//

import UIKit

class CustomTableViewCell: UITableViewCell {

//    override func awakeFromNib() {
//        super.awakeFromNib()
//        // Initialization code
//    }
//
//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//
//        // Configure the view for the selected state
//    }
    
    let image: UIImageView = {
        let image = UIImageView()
        image.backgroundColor = .red
        image.layer.borderColor = UIColor.black.cgColor
        image.image = UIImage(named: "profile")
        image.contentMode = .scaleAspectFill
        image.clipsToBounds = true
        //image.layer.borderWidth = 2
        return image
    }()
    
    let label: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 15)
        label.textColor = .black
        label.text = "This is a custom tabel view cell..."
        label.numberOfLines = 0
        return label
    }()
    
    let rightSwitch: UISwitch = {
        let iSwitch = UISwitch()
        iSwitch.isOn = true
        iSwitch.onTintColor = .red
        return iSwitch
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.backgroundColor = .white
        contentView.addSubview(image)
        contentView.addSubview(label)
        contentView.addSubview(rightSwitch)
        
        rightSwitch.addTarget(self, action: #selector(switchTapped), for: .valueChanged)
//        contentView.translatesAutoresizingMaskIntoConstraints = false
//        contentView.heightAnchor.constraint(equalToConstant: 70).isActive = true
        
    }
    
    @objc func switchTapped(){
        let vc = UIViewController()
        vc.view.backgroundColor = .blue
        print("Switch")
        
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        image.translatesAutoresizingMaskIntoConstraints = false
        
        contentView.layoutIfNeeded()
        NSLayoutConstraint.activate([
            image.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10),
            image.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.75),
            image.widthAnchor.constraint(equalTo: image.heightAnchor),
            image.centerYAnchor.constraint(equalTo: contentView.centerYAnchor)
        ])
        
        image.layoutIfNeeded()
        image.layer.cornerRadius = image.frame.height/2.0
        
        label.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            label.centerYAnchor.constraint(equalTo: image.centerYAnchor),
            label.leadingAnchor.constraint(equalTo: image.trailingAnchor, constant: 5),
            label.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 0.6)
        ])
        
        rightSwitch.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            rightSwitch.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -5),
            rightSwitch.centerYAnchor.constraint(equalTo: label.centerYAnchor),
            rightSwitch.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.3)
        ])
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
