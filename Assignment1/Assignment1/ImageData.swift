//
//  ImageData.swift
//  Assignment1
//
//  Created by Temp on 28/01/22.
//

import Foundation
import UIKit

struct ImageData{
    
    
    static func pickImage() -> String{
        let imageSet = ["spiderman","nssl0033","nssl0034","nssl0041","nssl0042","nssl0043","nssl0045","nssl0046","nssl0049","nssl0051","nssl0091"]
        
        if let image = imageSet.randomElement(){
            return image
        }else{
            return "spiderman"
        }
    }
}
