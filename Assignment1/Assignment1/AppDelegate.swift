//
//  AppDelegate.swift
//  Assignment1
//
//  Created by Temp on 28/01/22.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    let newWindow = UIWindow(frame: UIScreen.main.bounds)

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        
        let vc = ViewController()
        newWindow.rootViewController = vc
        newWindow.makeKeyAndVisible()
        return true
    }


}

