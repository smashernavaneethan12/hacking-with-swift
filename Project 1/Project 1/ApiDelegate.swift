//
//  ApiDelegate.swift
//  Project 1
//
//  Created by Temp on 31/01/22.
//

import Foundation

protocol ApiDelegate{
    func apiDidUpdated(fetchedData: [ApiData])
}
