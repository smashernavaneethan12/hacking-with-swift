//
//  Api.swift
//  Project 1
//
//  Created by Temp on 31/01/22.
//

import Foundation

struct Api{
    
    let urlString = "https://jsonplaceholder.typicode.com/photos"
    var delegate: ApiDelegate?
    
    func performRequest(){
        if let url = URL(string: urlString){
            
            let session = URLSession(configuration: .default)
            
            let task = session.dataTask(with: url, completionHandler: apiHandler(data:response:error:))
            
            task.resume()
        }
    }
    
    func apiHandler(data: Data?, response: URLResponse?, error: Error?){
        if error != nil {
            print(error!)
            return
        }else{
            if let safeData = data{
                if let resultData = parseJSON(jsonData: safeData){
                    print(resultData.count)
                    delegate?.apiDidUpdated(fetchedData: resultData)
                }
                
                
            }
        }
    }
    
    func parseJSON(jsonData: Data) -> [ApiData]?{
        let decoder = JSONDecoder()
        
        do{
            let imageData = try decoder.decode([ApiData].self, from: jsonData)
            return imageData
        }catch{
            print(error)
            return nil
        }
    }
}
