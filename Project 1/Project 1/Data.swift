//
//  Data.swift
//  Project 1
//
//  Created by Temp on 31/01/22.
//

import Foundation

struct ApiData: Codable{
    let albumId : Int
    let id: Int
    let title: String
    let url: String
    let thumbnailUrl: String
}
