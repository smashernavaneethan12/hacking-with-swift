//
//  SettingsViewController.swift
//  Project 1
//
//  Created by Temp on 31/01/22.
//

import UIKit

class SettingsViewController: UIViewController{
   
    let tableView = UITableView()
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Settings"
        tableView.delegate = self
        tableView.dataSource = self
        view.backgroundColor = .orange
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "settingsCell")
        view.addSubview(tableView)
        
    }
    override func viewDidLayoutSubviews() {
        tableView.frame = view.bounds
    }
    
}
//MARK: - TableViewDelegates
    
    extension SettingsViewController: UITableViewDelegate, UITableViewDataSource{
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 10
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell", for: indexPath)
            cell.textLabel?.text = "Hello"
            return cell
        }
    }
