//
//  AppDelegate.swift
//  Project 1
//
//  Created by Temp on 28/01/22.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var mainWindow: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let navBarAppearance = UINavigationBarAppearance()
        navBarAppearance.backgroundColor = .white
        UINavigationBar.appearance().standardAppearance = navBarAppearance
        UINavigationBar.appearance().scrollEdgeAppearance = navBarAppearance
        
        let tabBarAppearace = UITabBarAppearance()
        tabBarAppearace.backgroundColor = .white
        UITabBar.appearance().standardAppearance = tabBarAppearace
        UITabBar.appearance().scrollEdgeAppearance = tabBarAppearace
        
        mainWindow = UIWindow(frame: UIScreen.main.bounds)
        let vc1 = UINavigationController(rootViewController: ViewController())
        let vc2 = UINavigationController(rootViewController: SettingsViewController())
        vc1.tabBarItem.image = UIImage(systemName: "house")
        vc2.tabBarItem.image = UIImage(systemName: "gear")
        vc2.title = "Tools"
        let tabBarController = UITabBarController()
        tabBarController.setViewControllers([vc1, vc2], animated: true)
        
        mainWindow?.rootViewController = tabBarController
        mainWindow?.makeKeyAndVisible()
        return true
    }

}
