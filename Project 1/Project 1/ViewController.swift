//
//  ViewController.swift
//  Project 1
//
//  Created by Temp on 28/01/22.
//

import UIKit

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,ApiDelegate {
    
    var apiFetching = Api()
    var data: [ApiData] = []
    
    var pictures: [String] = []
    let sampleTableView = UITableView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        apiFetching.delegate = self
        view.backgroundColor = .white
        title = "Home"
        apiFetching.performRequest()
        sampleTableView.delegate = self
        sampleTableView.dataSource = self
        navigationItem.backButtonDisplayMode = .minimal
        
        
        let fm = FileManager.default
        let path = Bundle.main.resourcePath!
        let items = try! fm.contentsOfDirectory(atPath: path)
        
        sampleTableView.register(UITableViewCell.self, forCellReuseIdentifier: "Picture")
        view.addSubview(sampleTableView)
        for item in items{
            if item.hasPrefix("nssl"){
                // all files starting with nssl
                pictures.append(item)
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        sampleTableView.frame = view.bounds
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return data.count
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = sampleTableView.dequeueReusableCell(withIdentifier: "Picture", for: indexPath)
         cell.textLabel?.text = data[indexPath.row].title
         return cell
    }
    
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = DetailViewController()
         vc.text = data[indexPath.row].url
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func apiDidUpdated(fetchedData: [ApiData]) {
        DispatchQueue.main.async {
            self.data = fetchedData
            self.sampleTableView.reloadData()
        }
//        data = fetchedData
//        sampleTableView.reloadData()
    }
}

//MARK: - TableViewDelegate


