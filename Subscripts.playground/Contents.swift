import UIKit

////creating a readonly subscripts
struct Student{
    var phoneNumber: [Int]

    subscript (index: Int)->Int {
        get {
            if index < phoneNumber.count{
                return phoneNumber[index]
            }else{
                return 0
            }
        }
    }
}

var student1 = Student(phoneNumber: [987654,123445,34521,56783,1567253])
print(student1[4])
//student1[4] = 12234


//// creating a read-write subscript
//struct Student{
//    var phoneNumber: [Int]
//
//    subscript (index: Int)->Int{
//        get {
//            if index < phoneNumber.count{
//                return phoneNumber[index]
//            }else{
//                return 0
//            }
//        }
//        set{
//            if index < phoneNumber.count{
//            phoneNumber[index] = newValue
//            }else{
//                phoneNumber.append(newValue)
//            }
//        }
//    }
//}
//
//var student1 = Student(phoneNumber: [987654,123445,34521,56783,1567253])
//print(student1[2])
//student1[2] = 987654321
//print(student1[2])


//creating a subscript with more than one argument
struct Months{
    var month = ["jan":31,"feb":28,"mar":31,"apr":30,"may":31,"jun":30,"jul":31,"aug":31,"sep":30,"oct":31,"nov":30,"dec":31]
    
    var days = ["mon","tue","wed","thu","fri","sat","sun"]
    subscript(month value: String,day: Int)->String{
        get {
            print("Days in \(value) --> \(month[value]!)")
            return days[day]
        }
    }
}

var month = Months()
let day = month[month: "jan", 2]
print(day)

////subscript with no arguments
//struct New{
//     subscript()->Int{
//        get {
//            return 0
//        }
//    }
//}
//
//var new = New()
//print(new[])


//subscript overloading and type subscript
extension Int{
    subscript()->String{
        get{
            return "int"
        }
    }
    subscript(value: Int)->Int{
        get {
            return self*value
        }
    }
    
    static subscript(value: String)->Int{
        get{
            return Int(value)!
        }
    }
}

print(20[5])
print(Int["123"])


//required init
class A {
     required init(){
        print("a")
    }
}
class B: A {
    required init(){
        print("b")
    }
}

class C: A {
    
}

let obj1 = C()

//protocols
//
//@objc protocol Sample{
//    func printHello()
//    @objc optional func addTwo(a:Int, b:Int) -> Int
//}
//
//class NewSample: Sample {
//
//    func printHello() {
//        print("hello")
//    }
//
//}





@objc protocol Samples{
    var Arithmetic: String { get }
    func printHello()
    @objc optional func addTwo(a:Int, b:Int) -> Int
}

class NewSamples: Samples {
    var Arithmetic: String = ""
    
    
    func printHello() {
        print("hello")
        print(Arithmetic)
    }
    
//    func addTwo(a: Int, b: Int) -> Int {
//        return (a+b)
//    }
    
}


var new = NewSamples()
new.Arithmetic = "Addition"
new.printHello()

//print(new.addTwo(a: 5, b: 4))


// memory management

class MemoryManager1{
    
    var property1: MemoryManager2?
    
    init(){
        print("Memory Manager1 Class initalised")
    }
    
    deinit{
        print("Memory Manager1 deallocated")
    }
}

class MemoryManager2{
    
    var property2: MemoryManager1?
    
    init(){
        
        print("Memory Manager2 Class initalised")
    }
    
    deinit{
        print("Memory Manager2 deallocated")
    }
    
}

var memory1: MemoryManager1?
var memory2: MemoryManager2?
memory1 = MemoryManager1()


print(CFGetRetainCount(memory1))
//var mainmemory2 = MemoryManager2()
//var mainmemory1 = MemoryManager1()

//weak var memory2 = memory
//weak var memory3 = memory
//memory2 = memory
//memory2 = nil



