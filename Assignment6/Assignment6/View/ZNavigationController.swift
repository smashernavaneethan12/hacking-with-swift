//
//  ZSNavigationController.swift
//  Assignment6
//
//  Created by Temp on 07/04/22.
//

import Foundation
import UIKit

public class ZNavigationController: UINavigationController {

    public var supportedOrientation : UIInterfaceOrientationMask = .all
  
    public convenience init(rootViewController: UIViewController,supportedOrient: UIInterfaceOrientationMask = .portrait) {
       
        self.init(rootViewController: rootViewController)
        supportedOrientation = supportedOrient
//        navigationBar.shadowImage = UIImage()
        view.backgroundColor = ZColor.bgColorGray
        
        navigationBar.barTintColor = ZColor.navBarColor
//        if DeviceType.isIphone {
//
//            view.backgroundColor = ZColor.bgColorGray
//            navigationBar.barTintColor = ZColor.navBarColor
//        }
    }


    public convenience init(rootViewController: UIViewController,transparentBar: Bool) {
        
        self.init(rootViewController: rootViewController)
        if transparentBar {
            navigationBar.setBackgroundImage(UIImage(), for: .default)
            navigationBar.shadowImage = UIImage()
//            navigationBar.isTranslucent = false
            navigationBar.barTintColor = ZColor.navBarColor
        }
        if #available(iOS 15.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = ZColor.bgColorGray
            appearance.shadowImage = nil
            appearance.shadowColor = ZColor.bgColorGray
            navigationBar.standardAppearance = appearance;
            navigationBar.scrollEdgeAppearance = navigationController?.navigationBar.standardAppearance
        }
    }
    

    //MARK:- Rotation Handling
    #if APPCLIP
    override public var shouldAutorotate: Bool{
        return true
    }
    
    override public var supportedInterfaceOrientations: UIInterfaceOrientationMask{
        return SwiftUtils.isIpad ? .all : supportedOrientation
    }
    #endif

}
