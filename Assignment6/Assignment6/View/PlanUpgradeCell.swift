//
//  PlanUpgradeCell.swift
//  Assignment6
//
//  Created by Temp on 07/04/22.
//

import Foundation
import UIKit
import SnapKit

class PlanUpgradeCell: UITableViewCell{
    
    static let resuseIdentifier: String = "PlanUpgradeCell"
    
    let planBackgroundView: UIView = {
        let view = UIView()
        view.backgroundColor = ZColor.planUpgradeCellColor
        return view
    }()
    
    let planIcon: UIImageView = {
        let icon = UIImageView()
        icon.contentMode = .scaleAspectFit
        return icon
    }()
    
    let planHeaderLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 15)
        label.text = "Your Current Plan"
        label.textColor = .black
        return label
    }()
    
    let currentPlanLabel: UILabel = {
        let label = UILabel()
//        label.text = "STANDARD EDITION"
        label.font = UIFont.boldSystemFont(ofSize: 19)
        label.textColor = .black
        return label
    }()
    
    let upgradeButton: UIButton = {
        let button = UIButton()
//        button.backgroundColor = ZColor.buttonColor
//        button.setTitleColor(UIColor.white, for: .normal)
//        button.setTitle("Upgrade Now", for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        return button
    }()
    
    private var viewModel: PlanUpgradeCellViewModel!{
        didSet{
            planIcon.image = viewModel.planImage
            upgradeButton.backgroundColor = viewModel.upgradeButtonColor
            upgradeButton.setTitle(viewModel.upgradeButtonText, for: .normal)
            currentPlanLabel.text = viewModel.currentPlan
        }
    }
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        
    }
    
    convenience init(style: UITableViewCell.CellStyle, reuseIdentifier: String?, viewModel: PlanUpgradeCellViewModel) {
        self.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.viewModel = viewModel
        
    }
    
    func configureViewModel(viewModel: PlanUpgradeCellViewModel){
        self.viewModel = viewModel
        addingSubViews()
        snapKitAutoLayout()
    }
    
    func addingSubViews(){
        contentView.addSubview(planBackgroundView)
        planBackgroundView.addSubview(planIcon)
        planBackgroundView.addSubview(planHeaderLabel)
        planBackgroundView.addSubview(currentPlanLabel)
        planBackgroundView.addSubview(upgradeButton)
    }
    
    func snapKitAutoLayout(){
        
        contentView.layoutIfNeeded()
        
        planBackgroundView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(5)
            make.bottom.equalToSuperview().offset(-5)
            make.trailing.equalToSuperview().offset(-10)
            make.leading.equalToSuperview().offset(10)
        }
        planBackgroundView.layoutIfNeeded()
      
        planBackgroundView.layer.cornerRadius = 25
        
        planIcon.snp.makeConstraints { make in
            make.centerY.equalTo(planBackgroundView)
            make.leading.equalTo(planBackgroundView).offset(20)
            make.height.equalTo(planBackgroundView).multipliedBy(0.65)
            make.width.equalTo(planIcon.snp.height)
        }
//        planIcon.layer.borderColor = UIColor.white.cgColor
//        planIcon.layer.borderWidth = 2
        
        planHeaderLabel.snp.makeConstraints { make in
            make.leading.equalTo(planIcon.snp.trailing).offset(25)
            make.width.equalTo(planBackgroundView).multipliedBy(0.45)
            make.top.equalTo(planBackgroundView).offset(16)
        }
        
        currentPlanLabel.snp.makeConstraints { make in
            make.top.equalTo(planHeaderLabel.snp.bottom).offset(6)
            make.leading.equalTo(planIcon.snp.trailing).offset(25)
            make.width.equalTo(planBackgroundView).multipliedBy(0.8)
        }
        
        upgradeButton.snp.makeConstraints { make in
            make.leading.equalTo(planIcon.snp.trailing).offset(25)
            make.top.equalTo(currentPlanLabel.snp.bottom).offset(6)
            make.width.equalTo(planBackgroundView).multipliedBy(0.35)
        }
        upgradeButton.layoutIfNeeded()
        upgradeButton.layer.cornerRadius = upgradeButton.frame.height * 0.35
    
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
