//
//  ProfileCell.swift
//  Assignment6
//
//  Created by Temp on 08/04/22.
//

import Foundation
import UIKit

class ProfileCell: UITableViewCell{
    
    static var reuseID = "ProfileCell"
    
    private var viewModel: ProfileCellViewModel!{
        didSet{
            
            var content: UIListContentConfiguration = UIListContentConfiguration.accompaniedSidebarSubtitleCell()
            content.text = viewModel.title
            content.secondaryText = viewModel.subtitle
            content.image = viewModel.cellImage.resizeImage(targetSize: CGSize(width: 35, height: 35))
            if viewModel.title == "Sign Out"{
                accessoryType = .none
                content.textProperties.color = .red
            }else{
                content.textProperties.color = ZColor.textColor
            accessoryType = .disclosureIndicator
            }
            
            content.textProperties.font = UIFont.boldSystemFont(ofSize: 14)
            contentConfiguration = content
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
    }
    
    func configure(with viewModel: ProfileCellViewModel){
        
        self.viewModel = viewModel
        configureCell()
    }
    
    private func configureCell(){
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}


extension UIImage{
    func resizeImage(targetSize: CGSize) -> UIImage? {
        let size = self.size
        
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio, height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(origin: .zero, size: newSize)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        self.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
}
