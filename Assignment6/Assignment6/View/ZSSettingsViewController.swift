//
//  ZSSettingsViewController.swift
//  Assignment6
//
//  Created by Temp on 07/04/22.
//

import UIKit
import SnapKit

class ZSSettingsViewController: UIViewController {
    
    private let tableViewCellReuseId: String = "NormalTableViewCellId"

    let profileTable = UITableView()
    
    var settingsSections: [SettingsSections] = [.upgrade,.general,.about]
    
    var planCellViewModel: PlanUpgradeCellViewModel?
    
    let planModel: PlanUpgradeCellModel = {
    
        let model = PlanUpgradeCellModel(planImage: UIImage(named: "spidermanWhite")!, currentPlan: "STANDARD EDITION", upgradeButtonText: "Upgrade Now")
        return model
    }()
    
    let generalCellModel = ProfileCellModel(title: "General", subtitle: "Account, Preferences and etc ", cellImage: UIImage(named: "spidermanWhite")!)
    let siriShortcutsModel = ProfileCellModel(title: "Siri Shortcuts", subtitle: "A way to save time and effort ", cellImage: UIImage(named: "spidermanWhite")!)
    let securityAndPrivacyModel = ProfileCellModel(title: "Security and Privacy", subtitle: "Zoho's Privacy Commitments", cellImage: UIImage(named: "spidermanWhite")!)
    let AboutModel = ProfileCellModel(title: "About", subtitle: nil, cellImage: UIImage(named: "spidermanWhite")!)
    let feedbackModel = ProfileCellModel(title: "Feedback", subtitle: "Let us know how we can improve ", cellImage: UIImage(named: "spidermanWhite")!)
    let rateOurAppModel = ProfileCellModel(title: "Rate Our App", subtitle: "How was your experience with us? ", cellImage: UIImage(named: "spidermanWhite")!)
    let faqModel = ProfileCellModel(title: "FAQ", subtitle: nil, cellImage: UIImage(named: "spidermanWhite")!)
    let signOutModel = ProfileCellModel(title: "Sign Out", subtitle: nil, cellImage: UIImage(named: "spidermanWhite")!)
    
    lazy var generalSectionModel: [ProfileCellModel] = {
        let model = [generalCellModel,siriShortcutsModel,securityAndPrivacyModel]
        return model
    }()
    
    lazy var aboutSectionModel: [ProfileCellModel] = {
        let model = [AboutModel,feedbackModel,rateOurAppModel,faqModel,signOutModel]
        return model
    }()
    
    var generalSectionViewModels: [ProfileCellViewModel] = []
    var aboutSectionViewModels: [ProfileCellViewModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .systemBackground
        
        self.navigationItem.title = "Settings"
        
        let barButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addButtonTapped))
        self.navigationItem.rightBarButtonItem = barButton
        
        
        profileTable.delegate = self
        profileTable.dataSource = self
        profileTable.showsVerticalScrollIndicator = false
        planCellViewModel = PlanUpgradeCellViewModel(model: planModel)
        
        generalSectionModel.forEach { ProfileCellModel in
            let viewModel = ProfileCellViewModel(model: ProfileCellModel)
            generalSectionViewModels.append(viewModel)
        }
        
        aboutSectionModel.forEach { ProfileCellModel in
            let viewModel = ProfileCellViewModel(model: ProfileCellModel)
            aboutSectionViewModels.append(viewModel)
        }
        
        addSubViewsOnLoad()
        registerProfileTableCells()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        snapKitLayoutViews()
    }
    
    @objc func addButtonTapped() {
        let model = PlanUpgradeCellModel(planImage: UIImage(named: "spidermanWhite")!, currentPlan: "Enterprise EDITION", upgradeButtonText: "Upgrade Now")
        planCellViewModel = PlanUpgradeCellViewModel(model: model)
        profileTable.reloadData()
    }
    
//MARK: - Adding subviews
    func addSubViewsOnLoad(){
        view.addSubview(profileTable)
    }
    
//MARK: - layouting Views
    func snapKitLayoutViews(){
        
        profileTable.snp.makeConstraints { make in
            make.top.bottom.equalTo(view.safeAreaLayoutGuide)
            make.leading.equalTo(view.safeAreaLayoutGuide).offset(10)
            make.trailing.equalTo(view.safeAreaLayoutGuide).offset(-10)
        }
        
    }
    
//MARK: - Registering Cells
    
    func registerProfileTableCells(){
        profileTable.register(PlanUpgradeCell.self, forCellReuseIdentifier: PlanUpgradeCell.resuseIdentifier)
        profileTable.register(UITableViewCell.self, forCellReuseIdentifier: tableViewCellReuseId)
        profileTable.register(ProfileCell.self, forCellReuseIdentifier: ProfileCell.reuseID)
    }
    
}

//MARK: - Extension TableView Delegate Methods

extension ZSSettingsViewController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return settingsSections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section{
        case SettingsSections.upgrade.rawValue:
            return 1
        case SettingsSections.general.rawValue:
            return 3
        case SettingsSections.about.rawValue:
            return 5
        default:
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let planUpgradeCell = profileTable.dequeueReusableCell(withIdentifier: PlanUpgradeCell.resuseIdentifier) as? PlanUpgradeCell
        
        
        let subTitleCell = profileTable.dequeueReusableCell(withIdentifier: ProfileCell.reuseID) as? ProfileCell
        
        let defaultCell = UITableViewCell(style: .subtitle, reuseIdentifier: "Default")
        
        if indexPath.section == SettingsSections.upgrade.rawValue{
            planUpgradeCell?.configureViewModel(viewModel: planCellViewModel!)
            planUpgradeCell?.selectionStyle = .none
            return planUpgradeCell ?? defaultCell
            
        }else if indexPath.section == SettingsSections.general.rawValue{
            subTitleCell?.configure(with: generalSectionViewModels[indexPath.row])
//            subTitleCell?.textLabel?.text = "General"
            
            return subTitleCell ?? defaultCell
        }else if indexPath.section == SettingsSections.about.rawValue{
//            subTitleCell?.textLabel?.text = "About"
            subTitleCell?.configure(with: aboutSectionViewModels[indexPath.row])
            return subTitleCell ?? defaultCell
        }else{
            return defaultCell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.section{
        case SettingsSections.upgrade.rawValue:
            return 120
        case SettingsSections.general.rawValue:
            return 55
        case SettingsSections.about.rawValue:
            return 55
        default:
            return 55
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        switch section{
        case SettingsSections.upgrade.rawValue:
            return nil
        case SettingsSections.general.rawValue:
            return "GENERAL"
        case SettingsSections.about.rawValue:
            return "ABOUT"
        default:
            return nil
        }
    }
}
