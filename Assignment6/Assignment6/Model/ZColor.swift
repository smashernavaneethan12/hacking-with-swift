//
//  ZColor.swift
//  Assignment6
//
//  Created by Temp on 07/04/22.
//

import UIKit.UIColor

struct ZColor {
    public static let  greyColor =   UIColor(red: 0.59, green: 0.62, blue: 0.66, alpha: 1)
    public static var  bgColorDullBlack  =   UIColor(white: 0, alpha: 0.4)
    public static let  redColor =  UIColor(red:0.93, green:0.30, blue:0.24, alpha:1.00)
    public static let  greenColor =   UIColor(red:0.46, green:0.84, blue:0.45, alpha:1.00)
    public static let upgradeButtonColor = UIColor(red: 255/255, green: 1/255, blue: 143/255, alpha: 1)
    public static let planUpgradeCellColor = UIColor(red: 133/255, green: 247/255, blue: 221/255, alpha: 1)
    public static let textColor = UIColor(red: 10/255, green: 29/255, blue: 97/255, alpha: 1)
    public static let bgColorWhite = UIColor.setAppearance(dark: UIColor.secondarySystemBackground, light:  UIColor.white)
    public static let bgColorGray = UIColor.systemGroupedBackground
    public static var  bgColorDark  =   UIColor(white: 0, alpha: 0.9)
    public static let seperatorColor = UIColor.setAppearance(dark: UIColor.separator, light: UIColor(red:0.88, green:0.88, blue:0.89, alpha:1.00))
    public static let whiteColor = UIColor.white
    public static let blueColor = UIColor(red:0.19, green:0.48, blue:0.96, alpha:1.00)
    public static let tintColor = UIColor.setAppearance(dark: ZColor.whiteColor, light: ZColor.blueColor)
    public static let buttonColor = UIColor(red:0.18, green:0.48, blue:0.96, alpha:1.00)
    public static let primaryTextColor = UIColor.label
    public static let secondaryTextColorDark = UIColor.secondaryLabel
    public static let secondaryTextColorLight = UIColor.tertiaryLabel
    public static let listSelectionColor = UIColor.setAppearance(dark: UIColor(red:0.22, green:0.23, blue:0.24, alpha:1.00),light: UIColor(red: 0.86, green: 0.91, blue: 0.97, alpha: 1.00))
    public static var  navBarColor      =   ZColor.bgColorGray
    public static var  seperatorColorLight = UIColor.opaqueSeparator
    public static var  suggestionViewBgColor     = UIColor.tertiarySystemBackground


}

extension UIColor {
    public static func setAppearance(dark: UIColor,light: UIColor) -> UIColor {
        return UIColor {(trait) -> UIColor in
            if trait.userInterfaceStyle == .dark {
                return dark
            } else {
                return light
            }
        }
    }
    
    public static func setColor(dark: UIColor,light: UIColor) -> UIColor {
        if #available(iOS 13, *){
            return .setAppearance(dark: dark, light: light)
        } else {
            return light
        }
    }
    
    convenience init(hexString: String) {
        let scanner = Scanner(string: hexString)
        scanner.scanLocation = 0
        
        var rgbValue: UInt64 = 0
        
        scanner.scanHexInt64(&rgbValue)
        
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        self.init(
            red: CGFloat(r) / 0xff,
            green: CGFloat(g) / 0xff,
            blue: CGFloat(b) / 0xff,
            alpha: 1
            )
    }
}

