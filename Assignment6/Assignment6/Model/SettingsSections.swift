//
//  SettingsSections.swift
//  Assignment6
//
//  Created by Temp on 07/04/22.
//

import Foundation

enum SettingsSections: Int{
    case upgrade
    case general
    case about
}
