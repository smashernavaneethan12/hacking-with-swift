//
//  PlanUpgradeCellViewModel.swift
//  Assignment6
//
//  Created by Temp on 08/04/22.
//

import Foundation
import UIKit

struct PlanUpgradeCellModel{
    var planImage: UIImage
    var currentPlan: String
    var upgradeButtonText: String
    var upgradeButtonColor: UIColor
    
    
    init(planImage: UIImage, currentPlan: String, upgradeButtonText: String, upgradeButtonColor: UIColor = ZColor.upgradeButtonColor){
        self.planImage = planImage
        self.currentPlan = currentPlan
        self.upgradeButtonText = upgradeButtonText
        self.upgradeButtonColor = upgradeButtonColor
    }
}
