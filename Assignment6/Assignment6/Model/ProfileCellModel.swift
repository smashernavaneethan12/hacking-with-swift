//
//  ProfileCellModel.swift
//  Assignment6
//
//  Created by Temp on 08/04/22.
//

import Foundation
import UIKit

struct ProfileCellModel{
    var title: String
    var subtitle: String?
    var cellImage: UIImage
}
