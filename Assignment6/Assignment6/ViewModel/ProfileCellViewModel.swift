//
//  ProfileCellViewModel.swift
//  Assignment6
//
//  Created by Temp on 08/04/22.
//

import Foundation
import UIKit

class ProfileCellViewModel{

    var title: String
    var subtitle: String?
    var cellImage: UIImage
    
    
    init(model: ProfileCellModel){
        self.title = model.title
        subtitle = model.subtitle
        cellImage = model.cellImage
    }
}
