//
//  PlanUpgradeCellViewModel.swift
//  Assignment6
//
//  Created by Temp on 08/04/22.
//

import Foundation
import UIKit

class PlanUpgradeCellViewModel{
    
    var planImage: UIImage
    var currentPlan: String
    var upgradeButtonText: String
    var upgradeButtonColor: UIColor
    
    init(model: PlanUpgradeCellModel){
        self.planImage = model.planImage
        currentPlan = model.currentPlan
        upgradeButtonText = model.upgradeButtonText
        upgradeButtonColor = model.upgradeButtonColor
    }
    
    
}
