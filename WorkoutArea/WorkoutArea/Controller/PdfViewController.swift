//
//  PdfViewController.swift
//  WorkoutArea
//
//  Created by Temp on 30/03/22.
//

import Foundation
import UIKit
import PDFKit

class PdfViewController: UIViewController{
    
    let pdfView: PDFView = {
        let view = PDFView()
        view.frame = UIScreen.main.bounds
        return view
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelHandler))
        view.addSubview(pdfView)
        
    }
    
    
    @objc func cancelHandler(){
        self.dismiss(animated: true)
    }
}
