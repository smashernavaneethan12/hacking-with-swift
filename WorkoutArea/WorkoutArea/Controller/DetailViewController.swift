//
//  DetailViewController.swift
//  WorkoutArea
//
//  Created by Temp on 01/04/22.
//

import Foundation
import UIKit
import AMPopTip
import PDFKit

class DetailViewController: UIViewController, ZSResizeViewDelegate{
    
    let popTip: PopTip = {
        let pop = PopTip()
        pop.bubbleColor = .systemBlue
        return pop
    }()
    
    var signImage: UIImage? = UIImage(systemName: "house")
    
    let userdefaultX = UserDefaults.standard.double(forKey: "centreX")
    let userdefaultY = UserDefaults.standard.double(forKey: "centreY")
    let userdefaultHeight = UserDefaults.standard.double(forKey: "height")
    let userdefaultWidth = UserDefaults.standard.double(forKey: "width")
 
    lazy var zResizeView: ZSResizeView = {
        let view = ZSResizeView(frame: CGRect(origin: .zero, size: CGSize(width: 100, height: 100)))
        view.isUserInteractionEnabled = true
        view.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(gestureAction)))
      
        view.signImageView.image = signImage
//        view.backgroundColor = .red
        view.frame.size = CGSize(width: userdefaultWidth, height: userdefaultHeight)
        view.center = CGPoint(x: userdefaultX, y: userdefaultY)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureAction))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        view.addGestureRecognizer(tapGesture)
        view.viewShape = .square
        return view
    }()
    
   
    lazy var horizontalGridLinesLayer : CAShapeLayer = {
        let horizontalGridLinesLayer = CAShapeLayer()
        view.layer.addSublayer(horizontalGridLinesLayer)
        horizontalGridLinesLayer.opacity = 0
        horizontalGridLinesLayer.strokeColor = UIColor.lightGray.cgColor
        horizontalGridLinesLayer.lineWidth = 0.5
        horizontalGridLinesLayer.fillColor = nil
        horizontalGridLinesLayer.zPosition = 1000
        horizontalGridLinesLayer.lineDashPattern = [NSNumber(value: 7), NSNumber(value: 3)] // 7 is the length of dash, 3 is length of the gap.
        return horizontalGridLinesLayer
    }()
    
    lazy var verticalGridLinesLayer : CAShapeLayer = {
        let horizontalGridLinesLayer = CAShapeLayer()
        view.layer.addSublayer(horizontalGridLinesLayer)
        horizontalGridLinesLayer.opacity = 0
        horizontalGridLinesLayer.strokeColor = UIColor.lightGray.cgColor
        horizontalGridLinesLayer.lineWidth = 0.5
        horizontalGridLinesLayer.fillColor = nil
        horizontalGridLinesLayer.zPosition = 1000
        horizontalGridLinesLayer.lineDashPattern = [NSNumber(value: 7), NSNumber(value: 3)] // 7 is the length of dash, 3 is length of the gap.
        return horizontalGridLinesLayer
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .systemBackground
        
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveBarButtonPressed))
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelHandler))
        view.addSubview(zResizeView)
        zResizeView.delegate = self
        
        
        
//        print("initial Center: \(zResizeView.center)")
        
//        UserDefaults.standard.set(20, forKey: "centreX")
        
//        UserDefaults.standard.set(20, forKey: "centreY")
        
//        UserDefaults.standard.set(100.0, forKey: "height")
        
//        UserDefaults.standard.set(100.0, forKey: "width")
        
    }
    
    
    @objc func saveBarButtonPressed(){
        
        zResizeView.resizeViewLeft.isHidden = true
        zResizeView.resizeViewRight.isHidden = true
        
        let renderer = UIGraphicsImageRenderer(size: view.bounds.size)
        let image = renderer.image { ctx in
            view.drawHierarchy(in: view.bounds, afterScreenUpdates: true)
        }
        
        let image2 = renderer.image { context in
            view.drawHierarchy(in: view.bounds, afterScreenUpdates: true)
        }
        
        let pdfDocument = PDFDocument()
        
        guard let pdfPage = PDFPage(image: image) else { return }
        
        guard let pdfPage1 = PDFPage(image: image2) else { return }
        
        pdfDocument.insert(pdfPage, at: 0)
        pdfDocument.insert(pdfPage1, at: 1)
      
        let fm = FileManager.default
        
        let documentDirectoryUrl = fm.urls(for: .documentDirectory, in: .userDomainMask).first
        
        guard let signedDocumentUrl = documentDirectoryUrl?.appendingPathComponent("SignedPdf") else { return }
        do{
            try fm.createDirectory(at: signedDocumentUrl, withIntermediateDirectories: true)

        }catch{
            print("File Path Error \(error.localizedDescription)")
        }
        
        let signedPdfFilePath = signedDocumentUrl.appendingPathComponent("signed.pdf")
//        print(signedPdfFilePath)
        
        let signPdfData = pdfDocument.dataRepresentation()
        
        fm.createFile(atPath: signedPdfFilePath.path, contents: signPdfData)
        
        zResizeView.resizeViewLeft.isHidden = false
        zResizeView.resizeViewRight.isHidden = false
        
        self.cancelHandler()
    }
    
    deinit{
        print("Deinited Detail View Controller")
    }
    
    @objc func cancelHandler(){
        self.navigationController?.viewControllers.last?.dismiss(animated: true)
        self.dismiss(animated: true)
    }
    
    @objc func gestureAction(_ gesture: UIPanGestureRecognizer){
        if gesture.state == .began{
//            let translation = gesture.translation(in: self.view)
            zResizeView.initialCentre = gesture.view!.center
//            print("gesture began Center: \(translation)")
            
        }else if gesture.state == .changed{
            let translation = gesture.translation(in: self.view)
            let newCentre = CGPoint(x: zResizeView.initialCentre.x + translation.x, y: zResizeView.initialCentre.y + translation.y)
            zResizeView.center = newCentre
            popTip.hide()
            moveGrid()
//            print("gesture changed Center: \(translation)")
            
        }else if gesture.state == .ended{
//            let translation = gesture.translation(in: self.view)
//            print("gesture ended Center: \(translation)")
            hideGrid()
//            zResizeView.transform = CGAffineTransform.identity
            
        }
    }
    
    @objc func tapGestureAction(_ gesture: UITapGestureRecognizer){
        
        var text = "Tapped"
        
        switch zResizeView.viewShape{
        case .square:
            text = zResizeView.viewShape.rawValue
        case .rectangle:
            text = zResizeView.viewShape.rawValue
        case .circle:
            text = zResizeView.viewShape.rawValue
        case .triangle:
            text = zResizeView.viewShape.rawValue
        case .cone:
            text = zResizeView.viewShape.rawValue
        }
        popTip.show(text: text, direction: .up, maxWidth: 400, in: view, from: zResizeView.frame)
        
    }
    
    func moveGrid(){
        horizontalGridLinesLayer.path = UIBezierPath(rect: CGRect(x: -1, y: zResizeView.frame.origin.y, width: self.view.frame.width + 200, height: zResizeView.frame.height)).cgPath
        horizontalGridLinesLayer.opacity = 1
        
        verticalGridLinesLayer.path = UIBezierPath(rect: CGRect(x: zResizeView.frame.origin.x, y: -1, width: zResizeView.frame.width, height: self.view.frame.height + 20)).cgPath
        verticalGridLinesLayer.opacity = 1
    }
    
    func hideGrid(){
        let x = Double(zResizeView.center.x)
        let y = Double(zResizeView.center.y)
        let height = Double(zResizeView.frame.height)
        let width = Double(zResizeView.frame.width)
        
        UserDefaults.standard.set(x, forKey: "centreX")
        UserDefaults.standard.set(y, forKey: "centreY")
        UserDefaults.standard.set(height, forKey: "height")
        UserDefaults.standard.set(width, forKey: "width")
        
        horizontalGridLinesLayer.opacity = 0
        verticalGridLinesLayer.opacity = 0
    }
    
    func changeSignImage(with newImage: UIImage){
        self.signImage = newImage
        self.zResizeView.signImageView.image = newImage
    }
    
}


enum ViewShape: String{
    case rectangle
    case square
    case circle
    case triangle
    case cone
}
