//
//  ViewController.swift
//  WorkoutArea
//
//  Created by Temp on 15/03/22.
//

import UIKit
import SnapKit
import SafariServices
import AuthenticationServices

class ViewController: UIViewController {

    let imageView: UIImageView = {
        let view = UIImageView()
        view.backgroundColor = .gray
        view.image = UIImage(systemName: "house")
        return view
    }()
    
    lazy var tapButton: UIButton = {
        let button = UIButton()
        button.setTitle("Tap me", for: .normal)
        button.backgroundColor = .systemBlue
        button.addTarget(self, action: #selector(tapButtonPressed), for: .touchUpInside)
        return button
    }()
    
    let container: UIView = {
       let v = UIView()
        
        v.backgroundColor = .red
        return v
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        layoutViews()
        animateImageView()
        
        
        
        view.backgroundColor = UIColor(red:0.88, green:0.88, blue:0.89, alpha:1.00)
        
    }

    func layoutViews(){
        view.addSubview(imageView)
        imageView.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.height.equalTo(70)
            make.width.equalTo(70)
        }
        imageView.transform = CGAffineTransform(rotationAngle: -70)
        
        view.addSubview(tapButton)
        tapButton.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().offset(-60)
            make.width.equalToSuperview().offset(-50)
            make.height.equalTo(50)
        }
        
//        view.addSubview(container)
        
//        container.translatesAutoresizingMaskIntoConstraints = false
//
//        NSLayoutConstraint.activate([
//            container.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
//            container.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
//            container.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
//            container.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor)
//       ])
        
        
        
        let signInAppleButton  =  ASAuthorizationAppleIDButton(authorizationButtonType: .default, authorizationButtonStyle: .whiteOutline)
        signInAppleButton.addTarget(self, action: #selector(signInApple), for: UIControl.Event.touchUpInside)
        signInAppleButton.layer.borderWidth = 1
        signInAppleButton.layer.cornerRadius = 8
        signInAppleButton.backgroundColor = .white
        view.addSubview(signInAppleButton)
        
        signInAppleButton.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.top.equalTo(tapButton.snp.bottom).offset(16)
            make.size.equalTo(tapButton)
        }
        signInAppleButton.clipsToBounds = true
        
    }

    @objc func signInApple(){
        print("hello")
    }
    
    func animateImageView(){
        
        UIImageView.animate(withDuration: 2) {
            self.imageView.transform = CGAffineTransform(rotationAngle: 0).scaledBy(x: 2, y: 2)
//            self.imageView.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        }
    }
    
    @objc func tapButtonPressed(){
        
        let config = SFSafariViewController.Configuration()
        
        let string = "Hello with swift"
        print(string)
        print(string.replacingOccurrences(of: "swift", with: "swiftUI"))
        
        
        if let url = URL(string: "https://www.youtube.com/watch?v=LxKZf0LyRbA"){
        let vc = SFSafariViewController(url: url, configuration: config)
//                self.navigationController?.pushViewController(vc, animated: true)
//        vc.modalPresentationStyle = .fullScreen
//        vc.modalTransitionStyle = .coverVertical
        present(vc, animated: true)
        
       }
    }
}

