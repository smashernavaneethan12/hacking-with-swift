//
//  CustomPresentationController.swift
//  WorkoutArea
//
//  Created by Temp on 21/03/22.
//

import UIKit

class CustomPresentationController: UIPresentationController {
    
    lazy var dimmingView: UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.frame = CGRect(x: 0, y: 0, width: self.containerView!.frame.width, height: self.containerView!.frame.height)
        view.alpha = 0.4
        return view
    }()
    
    
    override init(presentedViewController: UIViewController, presenting presentingViewController: UIViewController?) {
        super.init(presentedViewController: presentedViewController, presenting: presentingViewController)
    }
    
    override var presentedView: UIView {
        return presentedViewController.view
    }
    
    override var frameOfPresentedViewInContainerView: CGRect {
        
        let height: CGFloat = 400
        
        return CGRect(x: 0, y: containerView!.frame.height - height, width: containerView!.frame.width, height: height)
        
    }
    
    override func containerViewDidLayoutSubviews() {
        super.containerViewDidLayoutSubviews()

        containerView?.insertSubview(dimmingView, at: 0)
        presentedView.frame = frameOfPresentedViewInContainerView
        containerView?.addSubview(presentedView)
        
    }
    
    
}
