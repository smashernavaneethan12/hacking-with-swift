//
//  ZContacts.swift
//  WorkoutArea
//
//  Created by Temp on 18/03/22.
//

import Foundation
import UIKit
import SnapKit
import Alamofire

var contacts: [ContactDetails] = []

let contactsUrl = "https://shielded-ridge-19050.herokuapp.com/api/?offset=1"

class ZContactsViewController: UIViewController{
    

    //Contacts array
    
    var searchedContacts:[ContactDetails] = []
    
    private let cellReuseID = "CellReuseId"
    
    let drawingViewController = ZSSignatureDrawingViewController()
    
    let detailVc = DetailViewController()
    
    var pickerColor: UIColor = .black
    
    let activityIndicatorView: UIActivityIndicatorView = {
        let activity = UIActivityIndicatorView()
        activity.color = .systemBlue
        activity.style = .large
        return activity
    }()
    
    // creating contacts tableView
    let tableView: UITableView = {
        let view = UITableView()
        return view
    }()
    
    // creating the search Bar
    lazy var searchBar: UISearchController = {
        let search = UISearchController()
        search.loadViewIfNeeded()
        search.obscuresBackgroundDuringPresentation = false
        search.delegate = self
        search.searchResultsUpdater = self
        search.searchBar.returnKeyType = UIReturnKeyType.done
        search.searchBar.placeholder = "Search Contacts"
        search.searchBar.autocorrectionType = .no
        search.searchBar.autocapitalizationType = .none
        return search
    }()
    
    // edit UiSwitch
    
    lazy var editSwitch: UISwitch = {
        let s = UISwitch()
        s.addTarget(self, action: #selector(editSwitchHandler), for: .valueChanged)
        return s
    }()
    
    let loadingView = LoadingView()
    
    // toolBar textField
    lazy var textView = UITextField()
    
    //MARK: - View LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        view.addSubview(tableView)
        view.addSubview(activityIndicatorView)
        view.addSubview(loadingView)
        
        contacts.removeAll()
        loadingView.isHidden = false
        ContactFetcher.shared.fetchContacts(from: contactsUrl, completionHandler: { cont in
            contacts = cont
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.loadingView.isHidden = true
            }
        })
        
        layoutView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellReuseID)
        self.navigationItem.searchController = searchBar
        
        
        
        self.navigationItem.hidesSearchBarWhenScrolling = false
        self.navigationController?.isToolbarHidden = false
        let addItem = UIBarButtonItem(barButtonSystemItem: .add,
                                      target: self,
                                      action: #selector(addToolBarItemHandler))
        
        textView.backgroundColor = .white
        textView.tintColor = .black
//        textView.frame = CGRect(x: 0, y: 0, width: (self.view.frame.width)*0.75, height: 40)
        
        textView.clearButtonMode = .whileEditing
        let spacerItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let refreshItem = UIBarButtonItem(barButtonSystemItem: .refresh,
                                          target: self,
                                          action: #selector(refreshTableview))
        tableView.keyboardDismissMode = .onDrag
        toolbarItems = [addItem,spacerItem,refreshItem]
        configureRefreshControl()
        
        let addBarButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addBarButtonHandler))
        self.navigationItem.rightBarButtonItem = addBarButton
        self.navigationItem.rightBarButtonItem?.isEnabled = false
        
        let leftBarButtonImage = UIImage(named: "paintBrush")?.resizeImage(targetSize: CGSize(width: 25, height: 25))
        
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: editSwitch)
        
//        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: leftBarButtonImage?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(pickerHandler))
        //        definesPresentationContext = true
        tableView.isEditing = true
        tableView.allowsSelectionDuringEditing = true
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
    }
    
    
    
//MARK: - Actions for Buttons
    
    @objc func editSwitchHandler(_ sender: UISwitch){
        self.tableView.reloadData()
    }
    
    func configureRefreshControl () {
        // Add the refresh control to your UIScrollView object.
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = .link
        tableView.refreshControl = refreshControl
        tableView.refreshControl?.addTarget(self,
                                            action: #selector(handleRefreshControl),
                                            for: .valueChanged)
        
        
    }
    
    @objc func handleRefreshControl() {
        // Update your content…
        
        self.tableView.reloadData()
        // Dismiss the refresh control.
        DispatchQueue.main.async {
            self.tableView.refreshControl?.endRefreshing()
        }
    }
    
    
    
    
    @objc func pickerHandler(){
        let colorPicker = UIColorPickerViewController()
        colorPicker.delegate = self
        
        self.present(colorPicker, animated: true)
        
    }
    
    
    // layouting subViews
    func layoutView(){
        
        
        tableView.snp.makeConstraints { make in
            make.leading.trailing.top.bottom.equalTo(view.safeAreaLayoutGuide)
        }
        
        activityIndicatorView.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
        
        loadingView.snp.makeConstraints { make in
            
            make.height.width.equalTo(100)
            make.center.equalToSuperview()
            
        }
        
    }
    
    @objc func addToolBarItemHandler(){
        self.navigationItem.rightBarButtonItem?.isEnabled = true
        
        let settingsOpenAlert = UIAlertController(title: "Open settings App?", message: nil, preferredStyle: .alert)
        
        settingsOpenAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else { return }
            
            UIApplication.shared.open(settingsUrl)
            
        }))
        
        settingsOpenAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(settingsOpenAlert, animated: true)
    }
    
    @objc func addBarButtonHandler(_ sender: UIBarButtonItem){
        
//        let vc = PopUpView()
//        vc.modalPresentationStyle = .custom
//        vc.transitioningDelegate = self
//        present(vc, animated: true)
        
        
        drawingViewController.delegate = self
        let nav = ZNavigationController(rootViewController: drawingViewController, supportedOrient: .landscapeLeft)
        
        nav.modalPresentationStyle = .fullScreen
        self.present(nav, animated: true)
        
        
    }
    
    @objc func refreshTableview(_ sender: UIBarButtonItem){
        self.tableView.reloadData()
        self.navigationItem.rightBarButtonItem?.isEnabled = false
    }
    
    
}




//MARK: - TableView Delegate and DataSource

extension ZContactsViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if searchBar.isSearching{
            return searchedContacts.count
        }else{
            return contacts.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: cellReuseID)
        
        if searchBar.isSearching{
            cell.textLabel?.text = searchedContacts[indexPath.row].name
            cell.textLabel?.textColor = pickerColor
            cell.detailTextLabel?.text = searchedContacts[indexPath.row].phone
        }else{
            cell.textLabel?.text = contacts[indexPath.row].name
            cell.textLabel?.textColor = pickerColor
            cell.detailTextLabel?.text = contacts[indexPath.row].phone
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if editSwitch.isOn{
            return true
        }else{
            return false
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let newAction = UITableViewRowAction(style: .default, title: "New") { action, indexPath in
            print("NewPressed")
        }
        newAction.backgroundColor = .blue
        
        
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete") { action, indexPath in
            
            contacts.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
        
        return [deleteAction,newAction]
    }
    
//    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
//        if !searchBar.isActive{
//            return .delete // setting a delete editing style for the row at indexPath
//        }else{
//            return .none
//        }
//    }
//
//    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
//
//        if !searchBar.isActive{
//            if editingStyle == .delete{
//                tableView.beginUpdates()
//                contacts.remove(at: indexPath.row)
//                tableView.deleteRows(at: [indexPath], with: .fade)
//                tableView.endUpdates()
//            }
//        }
//    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let nav = detailVc.embedInNavigation()
        nav.modalPresentationStyle = .fullScreen
        
        
        self.present(nav,animated: true)
    }
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {

        
        // getting the movable contact
        let contactToMove = contacts[sourceIndexPath.row]
        
        // removing the selected contact
        contacts.remove(at: sourceIndexPath.row)
        
        //inserting the moved contact into the position
        contacts.insert(contactToMove, at: destinationIndexPath.row)
        
    }
}

//MARK: - Search Controller Delegate

extension ZContactsViewController: UISearchControllerDelegate, UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        
        if searchBar.isActive{
            activityIndicatorView.startAnimating()
            searchedContacts.removeAll()
            if let searchedText = searchController.searchBar.text{
                for contact in contacts{
                    if contact.name.lowercased().contains(searchedText.lowercased()){
                        searchedContacts.append(contact)
                    }
                }
            }
            
        }else{
            searchedContacts.removeAll()
            
            
        }
        self.tableView.reloadData()
    }
    
    func didDismissSearchController(_ searchController: UISearchController) {
        searchedContacts.removeAll()
        activityIndicatorView.stopAnimating()
    }
}


//MARK: - SearchBar Exrtension

extension UISearchController{
    var isSearching: Bool {
        get {
            if let searchText = self.searchBar.text, searchText.isEmpty{
                return false
            }else{
                return true
            }
        }
        set{
            
        }
    }
}


extension ZContactsViewController: UIViewControllerTransitioningDelegate{
    
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        
        let presentationController = CustomPresentationController(presentedViewController: presented, presenting: presenting)
        return presentationController
    }
}


//MARK: - UIColorPickerDelegate

extension ZContactsViewController: UIColorPickerViewControllerDelegate{
    
    func colorPickerViewControllerDidFinish(_ viewController: UIColorPickerViewController) {
        print(viewController.selectedColor)
        
        
    }
    
    func colorPickerViewController(_ viewController: UIColorPickerViewController, didSelect color: UIColor, continuously: Bool) {
        pickerColor = viewController.selectedColor
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
}

extension ZContactsViewController:ZSSignatureDrawingViewControllerDelegate {
func didFinishSigning(signature: UIImage) {
//    detailVc.signImage = signature
    detailVc.changeSignImage(with: signature)
}
}

extension UIImage{
    func resizeImage(targetSize: CGSize) -> UIImage? {
        let size = self.size
        
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio, height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(origin: .zero, size: newSize)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        self.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
}
