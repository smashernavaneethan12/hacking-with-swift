//
//  ZSSignatureDrawingViewController.swift
//  WorkoutArea
//
//  Created by Temp on 13/04/22.
//

import Foundation
import UIKit
import PencilKit
import SnapKit

protocol ZSSignatureDrawingViewControllerDelegate{
    func didFinishSigning(signature: UIImage)
}

class ZSSignatureDrawingViewController: UIViewController, PKCanvasViewDelegate, PKToolPickerObserver{
    
    
    let canvasArea = PKCanvasView()
    let drawing = PKDrawing()
    let toolPicker = PKToolPicker()
    var allowScroll: Bool = false
    
    
    lazy var undoBarButton = UIBarButtonItem(barButtonSystemItem: .undo, target: self, action: #selector(undoPressed))
    
    lazy var redoBarButton = UIBarButtonItem(barButtonSystemItem: .redo, target: self, action: #selector(redoPressed))
    
    private let originSpace: CGFloat = 10
    private let sizeSpace: CGFloat = 20
    
    var delegate: ZSSignatureDrawingViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(canvasArea)
        
        canvasArea.backgroundColor = .lightGray
        canvasArea.delegate = self
        canvasArea.drawing = drawing
        
        canvasArea.alwaysBounceVertical = true
        canvasArea.drawingPolicy = .anyInput
        canvasArea.isScrollEnabled = true
        
        
        canvasArea.snp.makeConstraints { make in
            make.top.leading.trailing.equalToSuperview()
            make.bottom.equalToSuperview().offset(70)
            
        }
        
        
        
        let cancelBarButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelButtonPressed))
        
        self.undoBarButton.isEnabled = false
        
        self.redoBarButton.isEnabled = false
        
        self.navigationItem.leftBarButtonItems = [cancelBarButton,undoBarButton,redoBarButton]
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneButtonPressed))
        
        
    
    }
    
    @objc func undoPressed(){
        guard let undoManager = self.canvasArea.undoManager else { return }
        if undoManager.canUndo{
            undoManager.undo()
            updateBarButtons()
        }
    }
    
    @objc func redoPressed(){
        guard let undoManager = self.canvasArea.undoManager else { return }
        if undoManager.canRedo{
            undoManager.redo()
            updateBarButtons()
        }
    }
    
    func updateBarButtons(){
        guard let undoManager = self.canvasArea.undoManager else { return }
        if undoManager.canUndo{
            undoBarButton.isEnabled = true
        }else{
            undoBarButton.isEnabled = false
        }
        
        if undoManager.canRedo{
            redoBarButton.isEnabled = true
        }else{
            redoBarButton.isEnabled = false
        }
    }
    
    
    @objc func doneButtonPressed(_ sender: UIBarButtonItem){
//        if allowScroll{
//            canvasArea.drawingPolicy = .anyInput
//            allowScroll.toggle()
//        }else{
//            canvasArea.drawingPolicy = .pencilOnly
//            allowScroll.toggle()
//        }
        
        let drawingBounds = canvasArea.drawing.bounds
        
        let originX = drawingBounds.origin.x - originSpace > canvasArea.frame.origin.x ? drawingBounds.origin.x - originSpace : canvasArea.frame.origin.x
        let originY = drawingBounds.origin.y - originSpace > canvasArea.frame.origin.y ? drawingBounds.origin.y - originSpace : canvasArea.frame.origin.y
        let width = drawingBounds.width + sizeSpace < canvasArea.frame.width ? drawingBounds.width + sizeSpace : canvasArea.frame.width
        let height = drawingBounds.height + sizeSpace < canvasArea.frame.height ? drawingBounds.height + sizeSpace : canvasArea.frame.height
        
        let imageBounds = CGRect(x: originX, y: originY, width: width, height: height)
        
        
        let image = canvasArea.drawing.image(from: imageBounds, scale: 1.0)
        delegate?.didFinishSigning(signature: image)
        self.cancelButtonPressed()
//        traitCollection.performAsCurrent {
//
//        }
        
        self.canvasArea.drawing = PKDrawing()
        self.undoBarButton.isEnabled = false
        self.redoBarButton.isEnabled = false
        
    }
    
    @objc func cancelButtonPressed(){
        self.dismiss(animated: true)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        toolPicker.setVisible(true, forFirstResponder: canvasArea)
        toolPicker.addObserver(canvasArea)
        toolPicker.addObserver(self)
        
        canvasArea.becomeFirstResponder()
        
        print(toolPicker.isVisible)
        
//        self.canvasArea.tool = PKInkingTool(.pen, color: .black, width: 5)
        
    }
    
    func canvasViewDrawingDidChange(_ canvasView: PKCanvasView) {
        updateBarButtons()
    }
   
    override var prefersHomeIndicatorAutoHidden: Bool{
        return true
    }
    
    deinit {
        print("ZSSignatureDrawingViewController Deinited")
    }
    
    
    override var shouldAutorotate: Bool{
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask{
        return .landscapeLeft
    }
}

public class ZNavigationController: UINavigationController {

    public var supportedOrientation : UIInterfaceOrientationMask = .all
  
    public convenience init(rootViewController: UIViewController,supportedOrient: UIInterfaceOrientationMask = .portrait) {
       
        self.init(rootViewController: rootViewController)
        supportedOrientation = supportedOrient
//        navigationBar.shadowImage = UIImage()
        view.backgroundColor = .lightGray
        
        navigationBar.barTintColor = .tintColor
//        if DeviceType.isIphone {
//
//            view.backgroundColor = ZColor.bgColorGray
//            navigationBar.barTintColor = ZColor.navBarColor
//        }
    }


    public convenience init(rootViewController: UIViewController,transparentBar: Bool) {
        
        self.init(rootViewController: rootViewController)
        if transparentBar {
            navigationBar.setBackgroundImage(UIImage(), for: .default)
            navigationBar.shadowImage = UIImage()
//            navigationBar.isTranslucent = false
            navigationBar.barTintColor = .tintColor
        }
        if #available(iOS 15.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = .lightGray
            appearance.shadowImage = nil
            appearance.shadowColor = .lightGray
            navigationBar.standardAppearance = appearance;
            navigationBar.scrollEdgeAppearance = navigationController?.navigationBar.standardAppearance
        }
    }
    

    //MARK:- Rotation Handling
//    #if APPCLIP
//    override public var shouldAutorotate: Bool{
//        return true
//    }
    
    override public var supportedInterfaceOrientations: UIInterfaceOrientationMask{
        return supportedOrientation
    }
//    #endif

}
