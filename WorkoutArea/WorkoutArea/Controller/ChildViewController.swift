//
//  ChildViewController.swift
//  WorkoutArea
//
//  Created by Temp on 28/03/22.
//

import Foundation
import UIKit
import SnapKit


class ChildViewController: UIViewController{
    
    let imageView: UIImageView = {
        let view = UIImageView()
        view.backgroundColor = .lightGray
//        view.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        view.image = UIImage(named: "paintBrush")
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .orange
        view.addSubview(imageView)
        
    
        
        
        imageView.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview().multipliedBy(1.5)
            make.width.height.equalTo(50)
        }
//        imageView.translatesAutoresizingMaskIntoConstraints = false
////        imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
////        imageView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
//        imageView.heightAnchor.constraint(equalToConstant: 50).isActive = true
//        imageView.widthAnchor.constraint(equalToConstant: 50).isActive = true
//        imageView.snp.makeConstraints { make in
//            make.center.equalTo(view)
//            make.height.width.equalTo(50)
//        }
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        imageView.center = view.center
//
//    }
//

//    override func viewDidLayoutSubviews() {
//        super.viewDidLayoutSubviews()
//
//        let height = view.snp.height * 0.25
//    }
}
