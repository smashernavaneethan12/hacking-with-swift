//
//  ParentViewController.swift
//  WorkoutArea
//
//  Created by Temp on 28/03/22.
//

import Foundation
import UIKit
import SnapKit

class ParentViewController: UIViewController{
    
    let contentView: UIView = {
        let view = UIView()
//        view.backgroundColor = .red
        view.layer.cornerRadius = 15
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(contentView)
        view.backgroundColor = .white
        layoutSubViews()
        addContentView()
        
    }
    
    func layoutSubViews(){
        
        contentView.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top).offset(20)
            make.left.equalToSuperview().offset(20)
            make.width.equalTo(200)
            make.height.equalTo(400)
        }
        
    }
    
    func addContentView(){
        let childController = ChildViewController()
        addChild(childController)
        contentView.addSubview(childController.view)
        childController.view.snp.makeConstraints { make in
            make.top.leading.trailing.bottom.equalTo(contentView)
        }
        
        childController.didMove(toParent: self)
    }
    
}
