//
//  PopUpView.swift
//  WorkoutArea
//
//  Created by Temp on 21/03/22.
//

import Foundation
import UIKit
import SnapKit
import Lottie
import MobileCoreServices
import UniformTypeIdentifiers
import PDFKit

class PopUpView: UIViewController, UITextFieldDelegate{
    
    var hasSetPointOrigin = false
    var pointOrigin: CGPoint?
    
    let topDragger: UIView = {
        let dragger = UIView()
        dragger.backgroundColor = .black
        dragger.alpha = 0.7
        return dragger
    }()
    
    let textField: UITextField = {
        let field = UITextField()
        field.backgroundColor = .systemBackground
        field.placeholder = "Add new contact"
        field.textColor = .black
        return field
    }()
    
    let lotAnimation: AnimationView = {
        let view = AnimationView()
        view.animation = Animation.named("peace")
        view.loopMode = .loop
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    lazy var browseButton: UIButton = {
        let button = UIButton()
        button.setTitle("Browse", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .systemBlue
        button.frame = CGRect(x: 30, y: 30, width: 100, height: 60)
        button.layer.cornerRadius = button.frame.height / 2.0
        button.addTarget(self, action: #selector(browseButtonHandler), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .red
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(panGestureRecognizerAction))
        view.addSubview(browseButton)
        view.addGestureRecognizer(panGesture)
        
        textField.delegate = self
        setUpDragger()
        lotAnimation.play()
    }
    
    
    override func viewDidLayoutSubviews() {
        if !hasSetPointOrigin {
            hasSetPointOrigin = true
            pointOrigin = self.view.frame.origin
        }
    }
    
    func setUpDragger(){
        view.addSubview(topDragger)
        topDragger.translatesAutoresizingMaskIntoConstraints = false
        topDragger.layer.cornerRadius = 3
        NSLayoutConstraint.activate([
            topDragger.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            topDragger.topAnchor.constraint(equalTo: view.topAnchor, constant: 5),
            topDragger.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.4),
            topDragger.heightAnchor.constraint(equalToConstant: 5)
        ])
        
//        view.addSubview(textField)
//        textField.translatesAutoresizingMaskIntoConstraints = false
//        textField.layer.cornerRadius = 0
//        NSLayoutConstraint.activate([
//            textField.centerXAnchor.constraint(equalTo: view.centerXAnchor),
////            textField.topAnchor.constraint(equalTo: view.topAnchor, constant: 5),
//            textField.centerYAnchor.constraint(equalTo: view.centerYAnchor),
//            textField.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.4),
//            textField.heightAnchor.constraint(equalToConstant: 52)
//        ])
        
        
        view.addSubview(lotAnimation)
        lotAnimation.translatesAutoresizingMaskIntoConstraints = false
        lotAnimation.layer.cornerRadius = 0
        NSLayoutConstraint.activate([
            lotAnimation.centerXAnchor.constraint(equalTo: view.centerXAnchor),
//            textField.topAnchor.constraint(equalTo: view.topAnchor, constant: 5),
            lotAnimation.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            lotAnimation.widthAnchor.constraint(equalToConstant: 300),
            lotAnimation.heightAnchor.constraint(equalToConstant: 300)
        ])
}

    @objc func browseButtonHandler(){

        let documentPicker = UIDocumentPickerViewController(forOpeningContentTypes: [UTType.image,UTType.audio,UTType.pdf])
        documentPicker.delegate = self
//        let documentPickerNav = documentPicker.embedInNavigation()
        documentPicker.modalPresentationStyle = .fullScreen
        present(documentPicker, animated: true)
}
    
    
    @objc func panGestureRecognizerAction(sender: UIPanGestureRecognizer) {
        let translation = sender.translation(in: view)
        
        // Not allowing the user to drag the view upward
        guard translation.y >= 0 else { return }
        
        // setting x as 0 because we don't want users to move the frame side ways!! Only want straight up or down
        view.frame.origin = CGPoint(x: 0, y: self.pointOrigin!.y + translation.y)
        
        if sender.state == .ended {
            let dragVelocity = sender.velocity(in: view)
            if dragVelocity.y >= 1300 {
                self.dismiss(animated: true, completion: nil)
            } else {
                // Set back to original position of the view controller
                UIView.animate(withDuration: 0.3) {
                    self.view.frame.origin = self.pointOrigin ?? CGPoint(x: 0, y: 400)
                }
            }
        }
    }


    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        if let text = textField.text{
//            contacts.append(text)
            self.dismiss(animated: true)
        }
        
        return true
    }
    
}

//MARK: - UIDocumentPickerDelegate

extension PopUpView: UIDocumentPickerDelegate{
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
//        let fm = FileManager.default
        let document = PDFDocument(url: urls.first!)
        
        
        let pdfViewer = PdfViewController()
        pdfViewer.modalPresentationStyle = .fullScreen
        pdfViewer.pdfView.document = document
        
        let pdfNav = pdfViewer.embedInNavigation()
        pdfNav.modalPresentationStyle = .fullScreen
//        controller.navigationController?.pushViewController(pdfViewer, animated: true)
        present(pdfNav, animated: true)
    }
}


extension UIViewController{
    public func embedInNavigation() -> UINavigationController{
        return UINavigationController(rootViewController: self)
    }
}

