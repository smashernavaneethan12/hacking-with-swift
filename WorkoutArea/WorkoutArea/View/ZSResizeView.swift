//
//  ZSResizeView.swift
//  WorkoutArea
//
//  Created by Temp on 01/04/22.
//

import Foundation
import UIKit
import SnapKit


protocol ZSResizeViewDelegate{
    func moveGrid()
    func hideGrid()
}

class ZSResizeView: UIView, UIGestureRecognizerDelegate{
    
    var initialCentre: CGPoint = CGPoint(x: 0, y: 0)
    var newCentre: CGPoint = CGPoint(x: 0, y: 0)
    let resizeViewLeft  = UILabel()
    let resizeViewRight  = UILabel()
    
    var beginFrame = CGRect()
    var newFrame = CGRect()
    
    var delegate: ZSResizeViewDelegate?
    var isRightSwipe: Bool = false
    
    var initialLocation = CGPoint()
    var touchLocation = CGPoint()
    
    let stampMinWidth: CGFloat = 50.0
    let stampMinHeight: CGFloat = 20.0
    
    var viewShape: ViewShape = .square
    
    let signImageView = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
        
        backgroundColor = .clear
        
        addSubview(signImageView)
        signImageView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        
        addSubview(resizeViewLeft)
        resizeViewLeft.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview().offset(0)
            make.centerX.equalTo(0)
            make.width.height.equalTo(30)
        }
        
        addSubview(resizeViewRight)
        resizeViewRight.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview().offset(0)
            make.trailing.equalToSuperview().offset(15)
            make.width.height.equalTo(30)
        }
        
        let strokeTextAttributes = [NSAttributedString.Key.strokeColor : UIColor.white,
                                    NSAttributedString.Key.strokeWidth : -2] as [NSAttributedString.Key : Any]
        
        resizeViewLeft.textAlignment   =   .center
        resizeViewLeft.isUserInteractionEnabled = true
        resizeViewLeft.textColor = UIColor.red
        resizeViewLeft.layer.zPosition = 1003;
        resizeViewLeft.attributedText =  NSAttributedString(string: "•", attributes: strokeTextAttributes)
        resizeViewLeft.font = UIFont.systemFont(ofSize: 55, weight: .bold);
        
        
        let leftResizeGesture =  UIPanGestureRecognizer(target: self, action: #selector(resizeViewPanGesture(_:)))
        leftResizeGesture.delegate    =   self;
        resizeViewLeft.addGestureRecognizer(leftResizeGesture)
        
        resizeViewRight.textAlignment   =   .center
        resizeViewRight.isUserInteractionEnabled = true
        resizeViewRight.textColor = UIColor.red
        resizeViewRight.layer.zPosition = 1003;
        resizeViewRight.attributedText =  NSAttributedString(string: "•", attributes: strokeTextAttributes)
        resizeViewRight.font = UIFont.systemFont(ofSize: 55, weight: .bold);
        
        let rightResizeGesture =  UIPanGestureRecognizer(target: self, action: #selector(resizeViewPanGesture(_:)))
        leftResizeGesture.delegate    =   self;
        resizeViewRight.addGestureRecognizer(rightResizeGesture)
    }
    
    func changeCentre(to centre: CGPoint){
        self.center = centre
        self.newCentre = centre
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func resizeViewPanGesture(_ gesture: UIPanGestureRecognizer){
        touchLocation = gesture.location(in: self.superview)
         let isRightSwipe = gesture.view == resizeViewRight
//        print("called")
        switch gesture.state{
        case .began:
            beginFrame = self.frame
            initialLocation = touchLocation
            print("Touch Location \(touchLocation)")
        case .changed:
            changeResizeView(isRightSwipe: isRightSwipe)
            delegate?.moveGrid()
        case .ended:
            print("Ended")
            delegate?.hideGrid()
        default:
            print("default")
            
        }
    }
    
    func changeResizeView(isRightSwipe: Bool){
        
        
        let diffX = isRightSwipe ? (touchLocation.x - initialLocation.x) : (initialLocation.x - touchLocation.x)
        let diffY = touchLocation.y - initialLocation.y
//        print("initialTouch: \(initialLocation)")
//        print("TouchLocation: \(touchLocation)")
//        print("DiffX : \(diffX)  and  DiffY : \(diffY)")
        
        var newBounds = beginFrame
        
        if !isRightSwipe{
        newBounds.origin.x = beginFrame.origin.x - diffX
        }
        newBounds.size.width = beginFrame.size.width + diffX
        newBounds.size.height = beginFrame.size.height + diffY
        
        if newBounds.size.width < stampMinWidth{
            newBounds.size.width = stampMinWidth
        }
        if newBounds.size.height < stampMinHeight{
            newBounds.size.height = stampMinHeight
        }
//        newBounds.size.height = newBounds.width
        
        self.frame = newBounds
        
    }
}
