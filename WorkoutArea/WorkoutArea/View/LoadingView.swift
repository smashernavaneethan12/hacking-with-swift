//
//  LoadingView.swift
//  WorkoutArea
//
//  Created by Temp on 05/04/22.
//

import Foundation
import UIKit
import Lottie
import SnapKit

class LoadingView: UIView{
    
    let loadingAnimation: AnimationView = {
        let view = AnimationView()
        view.animation = Animation.named("Loading")
        view.loopMode = .loop
        return view
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(loadingAnimation)
        loadingAnimation.play()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        loadingAnimation.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
