//
//  ContactDetails.swift
//  WorkoutArea
//
//  Created by Temp on 05/04/22.
//

import Foundation

struct ContactDetails: Codable{
    var name: String
    var phone: String
}
