//
//  CotactFetcher.swift
//  WorkoutArea
//
//  Created by Temp on 05/04/22.
//

import Foundation

class ContactFetcher{
    static var shared = ContactFetcher()
    
    private init(){
        
    }
    
    func fetchContacts(from urlString: String, completionHandler: @escaping ([ContactDetails])->Void ){
        
        // creating the URL
        guard let contactUrl = URL(string: urlString) else {
            return
        }
        // creating the Request with HTTP method
        let request = try? URLRequest(url: contactUrl, method: .get, headers: nil)
        
        // creating the URLSession
        
        let session = URLSession(configuration: .default)
        
        // creating the dataTask

        let dataTask = session.dataTask(with: request!) { data, response, error in
            
            if error != nil {
                print("Fetch Error: \(error!)")
                return
            }
            
            guard let data = data else {
                return
            }

            let decoder = JSONDecoder()
            
            do {
                let contacts = try decoder.decode([ContactDetails].self, from: data)
                completionHandler(contacts)
            } catch {
                print(error)
            }
           
        }
        
        dataTask.resume()
    }
}

