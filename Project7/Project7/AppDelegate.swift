//
//  AppDelegate.swift
//  Project7
//
//  Created by Temp on 24/02/22.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        
        let navApperance = UINavigationBarAppearance()
        navApperance.backgroundColor = .white
        UINavigationBar.appearance().standardAppearance = navApperance
        UINavigationBar.appearance().scrollEdgeAppearance = navApperance
        
        let tabApperance = UITabBarAppearance()
        tabApperance.backgroundColor = .white
        UITabBar.appearance().standardAppearance = tabApperance
        UITabBar.appearance().scrollEdgeAppearance = tabApperance
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        let vc1 = ViewController()
        let vc2 = WebViewController()
        
        vc1.tabBarItem = UITabBarItem(tabBarSystemItem: .favorites, tag: 0)
        vc1.tabBarItem.badgeValue = "New"
        vc2.tabBarItem = UITabBarItem(title: "Web", image: UIImage(systemName: "house"), tag: 1)
        let nav1 = UINavigationController(rootViewController: vc1)
        let nav2 = UINavigationController(rootViewController: vc2)
        
        let tabBar = UITabBarController()
        tabBar.setViewControllers([nav1,nav2], animated: true)
        
        window?.rootViewController = tabBar
        
        return true
    }
    
    
    
}

