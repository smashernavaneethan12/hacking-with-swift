//
//  JsonDatas.swift
//  Project7
//
//  Created by Temp on 25/02/22.
//

import Foundation

struct Result: Codable{
    var results: [Petition]
}

struct Petition: Codable{
    var id: String
    var type: String
    var title: String
    var body: String
    var issues: [Issue]
    var signatureCount: Int
    var url: String
}

struct Issue: Codable {
    var id: Int
    var name: String
}


