//
//  ViewController.swift
//  Project7
//
//  Created by Temp on 24/02/22.
//

import UIKit
import WebKit
import SwiftUI


class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    
    //MARK: - Stored Properties
    var petitions = [Petition]()
    let tableView = UITableView()
    let cellIdentifer = "petitionCell"
    let urlString = "https://www.hackingwithswift.com/samples/petitions-2.json"
    //    var webUrl = "https://www.hackingwithswift.com/samples/petitions-2.json"
    var webUrl = "https://www.whitehouse.gov/"
    
    //MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Petitions"
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellIdentifer)
        view.addSubview(tableView)
        
        fetchData(urlString: urlString)
        addConstraint()
    }
    
    func parseJson(_ data: Data) -> Result?{
        let decoder = JSONDecoder()
        
        do{
            let decodedData = try decoder.decode(Result.self, from: data)
            return decodedData
        }catch{
            print(error)
            return nil
        }
    }
    
    //MARK: - JsonData fetching
    func fetchData(urlString: String){
        guard let url = URL(string: urlString) else
        {
            print("Invalid URL")
            return
        }
        let session = URLSession(configuration: .default)
        let task = session.dataTask(with: url) { data, response, error in
            if error != nil {
                print(error!)
                return
            }
            if let safeData = data{
                if let parsedData = self.parseJson(safeData){
                    DispatchQueue.global(qos: .background).async {[weak self] in
                        self?.petitions = parsedData.results
                        
                        DispatchQueue.main.async {
                            self?.tableView.reloadData()
                        }
                    }
                }
            }
        }
        task.resume()
    }
    
    //MARK: -  AutoLayout constraints
    func addConstraint(){
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor)
        ])
    }
    
    //MARK: - tableViewDelegate and datasource methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return petitions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //        let cell: UITableViewCell = {
        //                guard let cell = tableView.dequeueReusableCell(withIdentifier: "UITableViewCell") else {
        //                    // Never fails:
        //                    return UITableViewCell(style: UITableViewCell.CellStyle.subtitle, reuseIdentifier: cellIdentifer)
        //                }
        //                return cell
        //            }()
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: cellIdentifer)
        tableView.dequeueReusableCell(withIdentifier: cellIdentifer, for: indexPath)
        cell.textLabel?.text = petitions[indexPath.row].title
        cell.detailTextLabel?.text = petitions[indexPath.row].type
        cell.accessoryType = .disclosureIndicator
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let vc = ShowPetitionViewController()
        webUrl = petitions[indexPath.row].url
        vc.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "WEB", style: .plain, target: self, action: #selector(loadWebPage))
        vc.title = "Petition Count: \(petitions[indexPath.row].signatureCount)"
        vc.titleLabel.text = petitions[indexPath.row].title
        vc.petitionLabel.text = petitions[indexPath.row].body
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func loadWebPage(){
        let webVC = WebViewController()
        print(webUrl)
        webVC.urlString = webUrl
        navigationController?.pushViewController(webVC, animated: true)
    }
}


//MARK: - ShowPetitionViewController
class ShowPetitionViewController: UIViewController {
    
    //MARK: - storedProperties
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.textAlignment = .center
        return label
    }()
    
    let petitionLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        view.addSubview(titleLabel)
        view.addSubview(petitionLabel)
        
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: view.layoutMarginsGuide.leadingAnchor),
            titleLabel.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.1),
            titleLabel.trailingAnchor.constraint(equalTo: view.layoutMarginsGuide.trailingAnchor)
        ])
        
        petitionLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            petitionLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor),
            petitionLabel.leadingAnchor.constraint(equalTo: view.layoutMarginsGuide.leadingAnchor),
            petitionLabel.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            petitionLabel.trailingAnchor.constraint(equalTo: view.layoutMarginsGuide.trailingAnchor)
        ])
    }
    
}


class WebViewController: UIViewController, WKNavigationDelegate{
    
    
    let webView: WKWebView = {
        let view = WKWebView()
        return view
    }()
    
    var urlString = "https://www.youtube.com"
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        view.backgroundColor = .red
        webView.navigationDelegate = self
        view.addSubview(webView)
        
        if let url = URL(string: urlString){
            webView.load(URLRequest(url: url))
        }
        
        webView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            webView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            webView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            webView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            webView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor)
        ])
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        navigationItem.title = webView.title
    }
    
}


class newView: UIViewController{
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let button = UIButton()
        button.setTitle("Text", for: .normal)
        button.backgroundColor = .black
        button.addTarget(self, action: #selector(actionn), for: .touchUpInside)
        view.backgroundColor = .red
        view.addSubview(button)
        
//        button.center = view.center
        
        button.frame = CGRect(x: 50, y: 100, width: 100, height: 52)
    }
    
    
    @objc func actionn(){
        let vc = UIViewController()
        vc.view.backgroundColor = .black
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true, completion: nil)
    }
}
