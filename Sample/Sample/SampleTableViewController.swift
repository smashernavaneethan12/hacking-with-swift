//
//  SampleTableViewController.swift
//  Sample
//
//  Created by Temp on 28/01/22.
//

import UIKit

class SampleTableViewController: UITableViewController{
    
    var range = 1...20
    var sampleTableView = UITableView()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sampleTableView.dataSource = self
        sampleTableView.delegate = self
        title = "Table View"
        view.addSubview(sampleTableView)
        sampleTableView.register(UITableViewCell.self, forCellReuseIdentifier: "MyCells")
    }
    
}


//MARK: - UITableViewDataSource

extension SampleTableViewController{
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return range.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
   
       let cell = tableView.dequeueReusableCell(withIdentifier: "MyCells", for: indexPath)
       
       cell.textLabel!.text = "Cell text"
           
       return cell
    }
}
