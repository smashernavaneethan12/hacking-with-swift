//
//  AppDelegate.swift
//  Sample
//
//  Created by Temp on 28/01/22.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var mainWindow: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
       
        mainWindow = UIWindow(frame: UIScreen.main.bounds)
        
        let vc = SampleTableViewController()
        mainWindow?.rootViewController = vc
        mainWindow?.makeKeyAndVisible()
        
        
        return true
    }

 
}

