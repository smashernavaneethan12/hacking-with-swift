import UIKit

let semaphore = DispatchSemaphore(value: 0)

var sharedResource = ["a"]
DispatchQueue.global().async {
    print("Kid 1 - wait")
    semaphore.signal()
    print("Kid 1 - wait finished")
    sleep(5) // Kid 1 playing with iPad
    sharedResource.removeAll()
    semaphore.wait()
    print("Kid 1 - done with iPad")
}
DispatchQueue.global().async {
    print("Kid 2 - wait")
    semaphore.wait()
    print("Kid 2 - wait finished")
    sleep(5) // Kid 1 playing with iPad
    sharedResource += ["b","c","d"]
    semaphore.signal()
    print("Kid 2 - done with iPad")
}
DispatchQueue.global().async {
    print("Kid 3 - wait")
    semaphore.wait()
    print("Kid 3 - wait finished")
    sleep(1) // Kid 1 playing with iPad
    sharedResource.append("f")
    semaphore.signal()
    print("Kid 3 - done with iPad")
}
DispatchQueue.global().sync {
    semaphore.wait()
    print(sharedResource)
    semaphore.signal()
}


class Hello: UIViewController{
    var id: Int?
}

