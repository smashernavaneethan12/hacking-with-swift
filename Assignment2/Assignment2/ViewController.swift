//
//  ViewController.swift
//  Assignment2
//
//  Created by Temp on 31/01/22.
//

import UIKit

class OneController: UIViewController {
    
    let fifthVC = FourController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "ONE"
        view.backgroundColor = .green
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(presentFour))
        
    }
    
    @objc func presentFour(){
        
        //let fifthVC = FifthController()
        
        let fourNavVc = UINavigationController(rootViewController: fifthVC)

        fifthVC.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .close, target: self, action: #selector(dissmissFourVc))
        fifthVC.view.backgroundColor = .blue
        
        fourNavVc.modalPresentationStyle = .fullScreen
        present(fourNavVc, animated: true, completion: nil)
    }
    
    @objc func dissmissFourVc(){
        fifthVC.dismiss(animated: true, completion: nil)
    }

}

class TwoController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "TWO"
        view.backgroundColor = .red
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .play, target: self, action: #selector(pushView))
        
    }
    
    @objc func pushView(){
        let vc = FourController()
        navigationController?.pushViewController(vc, animated: true)
    }


}

class ThreeController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "THREE"
        view.backgroundColor = .yellow
        
    }


}

class FourController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "FOUR"
        view.backgroundColor = .orange
    }
        
}


