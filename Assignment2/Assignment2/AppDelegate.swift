//
//  AppDelegate.swift
//  Assignment2
//
//  Created by Temp on 31/01/22.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var mainWindow: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        mainWindow = UIWindow(frame: UIScreen.main.bounds)
        
        let appearance = UINavigationBarAppearance()
           appearance.configureWithOpaqueBackground()
        appearance.backgroundColor = .white
           UINavigationBar.appearance().standardAppearance = appearance
           UINavigationBar.appearance().scrollEdgeAppearance = appearance
        
        let tabBarAppearance = UITabBarAppearance()
        tabBarAppearance.backgroundColor = .white
        UITabBar.appearance().standardAppearance = tabBarAppearance
        UITabBar.appearance().scrollEdgeAppearance = tabBarAppearance
        
        let tabBarControl = UITabBarController()
        let oneVC = UINavigationController(rootViewController: OneController())
        let twoVC = UINavigationController(rootViewController: TwoController())
        let threeVC = UINavigationController(rootViewController: ThreeController())
        let fourVC = UINavigationController(rootViewController: FourController())
        oneVC.title = "HOME"
        twoVC.title = "MIC"
        threeVC.title = "DUSTBIN"
        fourVC.title = "KEYBOARD"
        oneVC.tabBarItem.image = UIImage(systemName: "house")
        twoVC.tabBarItem.image = UIImage(systemName: "mic")
        threeVC.tabBarItem.image = UIImage(systemName: "trash")
        fourVC.tabBarItem.image = UIImage(systemName: "keyboard")
        tabBarControl.setViewControllers([oneVC, twoVC, threeVC, fourVC], animated: true)
        
        mainWindow?.rootViewController = tabBarControl
        mainWindow?.makeKeyAndVisible()
        return true
    }



}

