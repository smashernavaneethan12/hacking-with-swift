//
//  ViewController.swift
//  Project2
//
//  Created by Temp on 23/02/22.
//

import UIKit

class ViewController: UIViewController {

    let flagButton1: UIButton = {
        let button = UIButton()
        //button.setImage(UIImage(named: "germany"), for: .normal)
        button.setTitleColor(.white, for: .selected)
        button.setTitleColor(.black, for: .normal)
        button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        button.tag = 0
        return button
    }()
    
    let flagButton2: UIButton = {
        let button = UIButton()
        //button.setImage(UIImage(named: "italy"), for: .normal)
        button.setTitleColor(.white, for: .selected)
        button.setTitleColor(.black, for: .normal)
        button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        button.tag = 1
        return button
    }()
    
    let flagButton3: UIButton = {
        let button = UIButton()
        //button.setImage(UIImage(named: "poland"), for: .normal)
        button.setTitleColor(.white, for: .selected)
        button.setTitleColor(.black, for: .normal)
        button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        button.tag = 2
        return button
    }()
    
    let verticalStack: UIStackView = {
        let stack = UIStackView()
        stack.distribution = .fillEqually
        stack.axis = .vertical
        return stack
    }()
    
    var flags = ["estonia","france","germany","ireland","italy","monaco","nigeria","poland","russia"]
    var correctAnswer = 0
    var score = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        //title = "Flags"
        view.backgroundColor = .red
        
        verticalStack.addArrangedSubview(flagButton1)
        verticalStack.addArrangedSubview(flagButton2)
        verticalStack.addArrangedSubview(flagButton3)
        
        view.addSubview(verticalStack)
        verticalStack.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            verticalStack.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            verticalStack.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            verticalStack.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            verticalStack.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor)
        ])
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(shareSheet))
        askQuestion()
    }
    
    @objc func shareSheet(){
        let image = UIImage(named: "germany")
        let url = URL(string: "https://www.google.com")
        let shareSheet = UIActivityViewController(activityItems: [url!,image!], applicationActivities: nil)
        shareSheet.popoverPresentationController?.barButtonItem = navigationItem.rightBarButtonItem
    
        present(shareSheet, animated: true)
    }
    
    func askQuestion(){
        flags.shuffle()
        correctAnswer = Int.random(in: 0...2)
        
        flagButton1.setImage(UIImage(named: flags[0]), for: .normal)
        flagButton2.setImage(UIImage(named: flags[1]), for: .normal)
        flagButton3.setImage(UIImage(named: flags[2]), for: .normal)
        
        title = flags[correctAnswer].uppercased()
    }

    @objc func buttonTapped(sender: UIButton){
        if sender.tag == correctAnswer{
            score+=1
            title = "Correct"
            print("Correct")
        }else{
            score-=1
            title = "Wrong"
            print("Wrong")
        }
        
        let ac = UIAlertController(title: "Score is \(score)", message: "Correct answer is \(correctAnswer+1)", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "Continue", style: .default, handler: { action in
            self.askQuestion()
        }))
        present(ac,animated: true)
    }
}

