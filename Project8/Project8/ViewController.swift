//
//  ViewController.swift
//  Project8
//
//  Created by Temp on 28/02/22.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    var stats: [Stat] = []
    var sampleArray: [String] = []
    var collectionView: UICollectionView?
    let collectionViewCellId = "collectionId"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 1
        layout.minimumInteritemSpacing = 1
        layout.sectionInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)

        
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addPicture))
        
        //sampleArray = UserDefaults.standard.stringArray(forKey: "stats") ?? []
        stats = UserDefaults.standard.array(forKey: "stats") as? [Stat] ?? []
        print(sampleArray)
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        guard let collectionView = collectionView else {
            return
        }

        //collectionView.backgroundColor = .red
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(CustomCollectionViewCell.self, forCellWithReuseIdentifier: collectionViewCellId)
        view.addSubview(collectionView)
        
    }

    override func viewDidLayoutSubviews() {
        
        guard let collectionView = collectionView else {
            return
        }

        collectionView.translatesAutoresizingMaskIntoConstraints = false
//        view.layoutIfNeeded()
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            collectionView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            collectionView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            collectionView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor)
        ])
    }

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return stats.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let collectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: collectionViewCellId, for: indexPath) as? CustomCollectionViewCell else { fatalError() }
    
        let stat = stats[indexPath.item]
        collectionCell.statImage.image = stat.image
        collectionCell.statLabel.text = stat.tex

        return collectionCell
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//        return 1
//    }
//
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        return 1
//    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 150, height: 300)
    }
    
    
    @objc func addPicture(){
        let picker = UIImagePickerController()
        //picker.sourceType = .photoLibrary
        picker.delegate = self
        picker.allowsEditing = true
        picker.modalPresentationStyle = .fullScreen
        present(picker, animated: true)
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let image = info[.editedImage] as? UIImage {
            let stat = Stat(image: image, tex: "Unknown")
            stats.append(stat)
            //sampleArray.append("unkown")
            
            UserDefaults.standard.setValue(stats , forKey: "stats")
        }
        collectionView?.reloadData()

        picker.dismiss(animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
        let ac = UIAlertController(title: "Name", message: nil, preferredStyle: .alert)
        ac.addTextField { textField in
            textField.placeholder = "Change Name"
        }
        ac.addAction(UIAlertAction(title: "Done", style: .default, handler: { [weak self]action in
            self?.stats[indexPath.item].tex = (ac.textFields?.first?.text)!
            self?.collectionView?.reloadData()
        }))
        ac.addAction(UIAlertAction(title: "cancel", style: .cancel, handler: nil))
        present(ac, animated: true)
    }
}

