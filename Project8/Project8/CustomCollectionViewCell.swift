//
//  CustomCollectionViewCell.swift
//  Project8
//
//  Created by Temp on 28/02/22.
//

import UIKit

class CustomCollectionViewCell: UICollectionViewCell {
    
    let statImage: UIImageView = {
        let image = UIImageView()
        image.layer.cornerRadius = 10
        image.clipsToBounds = true
        image.layer.borderColor = UIColor.black.cgColor
        image.layer.borderWidth = 2
        //image.backgroundColor = .black
        return image
    }()
    
    let statLabel: UILabel = {
        let label = UILabel()
        label.text = "Label"
        //label.backgroundColor = .white
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 15)
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        contentView.backgroundColor = .lightGray
        contentView.layer.cornerRadius = 5
        
    }
    
    override func layoutSubviews() {
        contentView.layoutIfNeeded()
        
        contentView.addSubview(statImage)
        statImage.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            statImage.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            statImage.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 30),
            statImage.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.5),
            statImage.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 0.7)
        ])
        
        contentView.addSubview(statLabel)
        statLabel.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            statLabel.topAnchor.constraint(equalTo: statImage.bottomAnchor, constant: 15),
            statLabel.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.2),
            statLabel.centerXAnchor.constraint(equalTo: statImage.centerXAnchor),
            statLabel.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 0.6)
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
