//
//  ViewController.swift
//  Assignment1AutoLayout
//
//  Created by Temp on 16/02/22.
//

import UIKit


struct ImageData{
    let imageSet = ["spiderman","nssl0033","nssl0034","nssl0041","nssl0042","nssl0043","nssl0045","nssl0046","nssl0049","nssl0051","nssl0091"]
    
    func pickImage() -> String{
        if let image = imageSet.randomElement(){
            return image
        }else{
            return "spiderman"
        }
    }
}



class ViewController: UIViewController {
    
    let data = ImageData()
    var myImage: String = "spiderman"
    let borderSize: CGFloat = 3
    let squareView = UIView()
    let roundView = UIView()
    let textLabel = UILabel(frame: CGRect(x: 20, y: 270, width: 150, height: 52))
    let blackButton = UIButton(frame: CGRect(x: 190, y: 270, width: 100, height: 52))
    let redButton = UIButton(frame: CGRect(x: 300, y: 270, width: 100, height: 52))
    let textField = UITextField(frame: CGRect(x: 75, y: 350, width: 250, height: 52))
    let imageView = UIImageView(frame: CGRect(x: 50, y: 450, width: 200, height: 200))
    let loadImg = UIButton(frame: CGRect(x: 270, y: 525, width: 125, height: 52))
    let toggleSwitch = UISwitch()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        textField.delegate = self
        
        
        squareView.frame = CGRect(x: 155, y: 50, width: 100, height: 100)
        squareView.backgroundColor = .yellow
        squareView.layer.borderWidth = borderSize
        squareView.layer.borderColor = UIColor.link.cgColor
        view.addSubview(squareView)
        
        
        roundView.frame = CGRect(x: 155, y: 160, width: 100, height: 100)
        roundView.layer.cornerRadius = 50
        roundView.backgroundColor = .link
        roundView.layer.borderWidth = borderSize
        roundView.layer.borderColor = UIColor.red.cgColor
        view.addSubview(roundView)
        
        
        textLabel.text = "Text Label"
        textLabel.textColor = .black
        textLabel.backgroundColor = .white
        textLabel.layer.borderColor = UIColor.red.cgColor
        textLabel.layer.borderWidth = 2
        textLabel.textAlignment = .center
        view.addSubview(textLabel)
        
        blackButton.backgroundColor = .black
        blackButton.setTitle("Black", for: .normal)
        blackButton.setTitleColor(UIColor.white, for: .normal)
        view.addSubview(blackButton)
        
        redButton.backgroundColor = .black
        redButton.setTitle("Red", for: .normal)
        redButton.setTitleColor(UIColor.white, for: .normal)
        view.addSubview(redButton)
        
    
        textField.placeholder = "Enter Value"
        textField.textAlignment = .center
        textField.layer.borderColor = UIColor.black.cgColor
        textField.layer.borderWidth = 2
        textField.textColor = .black
        textField.keyboardType = UIKeyboardType.default
        textField.returnKeyType = UIReturnKeyType.done
        view.addSubview(textField)
        
        imageView.layer.borderWidth = 2
        imageView.layer.borderColor = UIColor.green.cgColor
        imageView.backgroundColor = .lightGray
        imageView.contentMode = .scaleAspectFit
        view.addSubview(imageView)
        
        loadImg.backgroundColor = .black
        loadImg.setTitle("LOAD IMAGE", for: .normal)
        loadImg.setTitleColor(UIColor.white, for: .normal)
        view.addSubview(loadImg)
        
        toggleSwitch.frame = CGRect(x: 125, y: 675, width: 0,height: 0)
        toggleSwitch.isOn = true
        view.addSubview(toggleSwitch)
                
        
        
        blackButton.addTarget(self, action: #selector(self.blackButtonPressed), for: .touchUpInside)
        redButton.addTarget(self, action: #selector(self.redButtonPressed), for: .touchUpInside)
        loadImg.addTarget(self, action: #selector(loadImgPressed), for: .touchUpInside)
        toggleSwitch.addTarget(self, action: #selector(toggleSwitchToggled), for: .valueChanged)
    }
    
    
}

//MARK: - ButtonActions

extension ViewController{
    @objc func blackButtonPressed(sender: UIButton){
        textLabel.textColor = .black
    }
    
    @objc func redButtonPressed(sender: UIButton){
        textLabel.textColor = .red
    }
    
    @objc func loadImgPressed(sender: UIButton){
        myImage = data.pickImage()
        if toggleSwitch.isOn{
            imageView.image = UIImage(named: myImage)
        }else{
            imageView.image = UIImage(named: myImage+"Black")
        }
    }
    
    @objc func toggleSwitchToggled(sender: UISwitch){
        
        if sender.isOn{
            imageView.image = UIImage(named: myImage)
        }else{
            imageView.image = UIImage(named: myImage+"Black")
        }
    }
}

//MARK: - textFieldDelegateMethods

extension ViewController: UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textLabel.text = textField.text
        textField.resignFirstResponder()
        return true
    }
    
    
}

